#!/bin/bash
#compile all exercises with and without solutions
for fol in Exercises_[1-9] Exam_example_questions; do
	cd $fol

	#compile with solutions
	sed -i 's/usepackage\[withoutSolutions\]{optional}/usepackage\[withSolutions\]{optional}/' ${fol}.tex
	pdflatex ${fol}.tex
	mv ${fol}.pdf ${fol}_solutions.pdf

	#compile without solutions
	sed -i 's/usepackage\[withSolutions\]{optional}/usepackage\[withoutSolutions\]{optional}/' ${fol}.tex
	pdflatex ${fol}.tex
	
	#clean up
	rm *.aux *.log
	cd ..
done

