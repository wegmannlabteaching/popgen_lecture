\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

\usepackage[withoutSolutions]{optional}


\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.2cm}\\ {\color{red} \texttt{#1}}}\bigskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}


%opening
\title{BL.0047 Population Genetics - Exercises 8}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{Deterministic Selection}
\begin{enumerate}[1.]
\item Suppose there is an allele $A$ in a haploid species at frequency $f_A=0.2$ and that individuals carrying $A$ have a 5\% higher fecundity than individuals carrying the alternative $a$ allele. \textit{(Adapted from exercises 7.1 \& 7.3 from Nielsen \& Slatkin)}

\begin{enumerate}[a)]
\item  What is the frequency $f_A(1)$ after one generation?
\sol{From the text we know that $\lambda_A = 1.050 \lambda_a$, and hence that $\frac{\lambda_a}{\lambda_A} = 1-s = 0.952$.\\ Thus, $f_A(1) =  \displaystyle \frac{f_A(0)}{f_A(0) + \displaystyle \frac{\lambda_a}{\lambda_A} f_a(0)} = \displaystyle \frac{0.2}{0.2 + 0.952 \cdot 0.8} = 0.208$.}

\item How many generations $t$ will it take for the $A$ allele to increase to $f_A(t)=0.8$?
\sol{We have $f_A(t)= \displaystyle \frac{f_A(0)}{f_A(0) + (1-s)^t f_a(0)} = 0.8$. The goal is now to solve this equation for $t$:
\begin{eqnarray*}
 f_A(t)&=& \displaystyle \frac{f_A(0)}{f_A(0) + (1-s)^t f_a(0)}\\
 f_A(t)f_A(0) + f_A(t) (1-s)^t f_a(0) &=& f_A(0)\\
 (1-s)^t &=& \displaystyle \frac{f_A(0)(1 - f_A(t))}{f_A(t)f_a(0)} = \displaystyle \frac{f_A(0)f_a(t)}{f_A(t)f_a(0)}\\
 t \log (1-s) &=& \log \left( \displaystyle \frac{f_A(0)f_a(t)}{f_A(t)f_a(0)} \right)\\
 t &=& \displaystyle \frac{1}{\log (1-s)} \log \left( \displaystyle \frac{f_A(0)f_a(t)}{f_A(t)f_a(0)} \right)
\end{eqnarray*}
Setting $f_A(0) = 0.2$, $f_a(0) = 0.8$, $f_A(t) = 0.8$, $f_a(t)=0.2$ and $1-s=0.952$ we get \medskip \\
$t = \displaystyle \frac{1}{\log 0.952} \log \left( \displaystyle \frac{0.2\cdot 0.2}{0.8 \cdot 0.8} \right) = \frac{\log(0.0625)}{\log 0.952} = 56.365$ generations. \medskip \\Since generations are integer numbers, it thus takes $57$ generation for $f_A(t) \geq 0.8$ with this extremely strong selective advantage.}

\item Suppose that individuals carrying allele $A$ have a 1\% lower fecundity than individuals carrying allele $a$. How many generations will it take to decrease from $f_A(0)=0.1$ to $f_A(t)=0.01$?
\sol{Here, $\lambda_A = 0.99 \lambda_a$, and hence $\frac{\lambda_a}{\lambda_A} = 1-s = 1.01$. Using the formula derived above, we get \medskip \\
$t = \displaystyle \frac{1}{\log 1.01} \log \left( \displaystyle \frac{0.1\cdot 0.99}{0.01 \cdot 0.9} \right) = \frac{\log(11)}{\log 1.01} = 240.99$ generations. \\ Since generations are integer numbers, it thus takes $241$ generation for $f_A(t) \leq 0.01$. }

\end{enumerate}

%---------------------
\item Cystic fibrosis (CF) is a Mendelian recessive disease of humans caused by a defect in the ion transport. Until 1950, when antibiotics were first used to treat the disease, most newborn died at an early age. Yet CF is relatively common in Caucasians, with a frequency at birth of 1/2500, which implies that the CF-causing mutation has a frequency of about 0.02 in the population. This is a surprisingly high frequency for an allele that was lethal to homozygots until very recently. The reason for this high frequency is still unclear. \textit{(Adapted from exercises 7.4 from Nielsen \& Slatkin)}

\begin{enumerate}[a)]
\item Suppose that the allele causing CF is maintained in the population by heterozygot advantage. In order for the equilibrium frequency to be $f_a= 0.02$, what would the selection coefficient of the homozygot healthy genotype have to be?
\sol{The equilibrium frequency is given by $\hat{f}_a = \frac{s_{AA}}{s_{AA} + s_{aa}}$, where $\frac{\upsilon_{aa}}{\upsilon_{Aa}} = 1-s_{aa}$ and $\frac{\upsilon_{AA}}{\upsilon_{Aa}} = 1-s_{AA}$.\medskip\\ Since the disease is lethal for homozygous individuals, $1-s_{aa} = 0$, and thus $s_{aa} = 1$.\medskip\\
We thus have $\hat{f}_a = 0.02 = \frac{s_{AA}}{s_{AA} + 1}$. Solving for $s_{AA}$ we get \begin{eqnarray*}
0.02(s_{AA} + 1) &=& s_{AA}\\                                                                                          
s_{AA}(1 - 0.02) &=& 0.02\\
s_{AA} &=& \frac{0.02}{0.98} = 0.024
\end{eqnarray*}}

\item Suppose that the allele causing CF is maintained in the population by mutation-selection balance. What would the necessary mutation rate have to be to maintain the allele at a frequency of 0.02? 
\sol{Under mutation-selection balance, $\hat{f}_a = \sqrt{\frac{\mu}{s_{aa}}}$. Since $s_{aa} = 1$, $\hat{f}_a = \sqrt{\mu}$ and $\mu = \hat{f}_a^2 = 0.0004$. Assuming that $\mu_{b} = 10^{-8}$ per base pair, this would imply that a total of 40,000 sites could mutate to cause CF, which is rather unlikely.} 

\end{enumerate}
%---------------------

\item Suppose that a previously uninhabited island is colonized by a green lizard species and the population grows to $2N=10^4$. Then, at one locus, a mutation $b \rightarrow B$ occurs in one individual that changes the color of these lizards to brown, which is less conspicuous on the brownish rocks of the island. Consequently, the viability of green individuals is 10\% lower than the viability of brown individuals.

\begin{enumerate}[a)]
\item Assume that the brown phenotype is dominant (and hence that the first individual heterozygous for it was brown). What is the selection coefficient against the homozygous green individuals $s_{bb}$?
\sol{In the dominant case, $\upsilon_{BB} = \upsilon_{Bb}$ and $\frac{\upsilon_{bb}}{\upsilon_{BB}} = 1-s$. In this case, $\frac{\upsilon_{bb}}{\upsilon_{BB}} = 0.9 = 1-s$, and hence $s = 0.1$.}

\item What is the expected allele frequency in the absence of genetic drift after one generation if the brown phenotype was dominant?
\sol{In the dominant case, $f_B(t+1) = \frac{f_B(t)^2 + f_B(t)(1-f_B(t))}{f_B(t)^2 + 2f_B(t)(1-f_B(t)) + (1-s)(1-f_B(t))^2}$.\\ \\
Since the population is of size $2N=10^4$ and there is currently a single copy of allele $B$ in the population, $f_B(t)=\frac{1}{10^4}$. Further, $s = 0.1$ (see above).\\ \\
We thus have $f_B(t+1) = \displaystyle \frac{\left(\frac{1}{10^4}\right)^2 + \frac{1}{10^4}\frac{10^4 - 1}{10^4}}{\left(\frac{1}{10^4}\right)^2 + 2\frac{1}{10^4}\frac{10^4 - 1}{10^4}) + 0.9 \left(\frac{10^4-1}{10^4}\right)^2} = 1.11110\cdot 10^{-4}$, which is in increase of about 10\%.}

\item What is the expected allele frequency in the absence of genetic drift after one generation if the brown phenotype was recessive?
\sol{In the recessive case, $f_B(t+1) = \frac{f_B(t)^2 + (1-s)f_B(t)(1-f_B(t))}{f_B(t)^2 + 2(1-s)f_B(t)(1-f_B(t)) + (1-s)(1-f_B(t))^2}$.\\ \\
Since the population is of size $2N=10^4$ and there is currently a single copy of allele $B$ in the population, $f_B(t)=\frac{1}{10^4}$. Further, $s = 0.1$ (see above).\\ \\
We thus have $f_B(t+1) = \displaystyle \frac{\left(\frac{1}{10^4}\right)^2 + 0.9\frac{1}{10^4}\frac{10^4 - 1}{10^4}}{\left(\frac{1}{10^4}\right)^2 + 2\cdot 0.9\frac{1}{10^4}\frac{10^4 - 1}{10^4}) + 0.9 \left(\frac{10^4-1}{10^4}\right)^2} =  1.00001\cdot 10^{-4}$, which i much less than in the dominant case.}

\end{enumerate}
%---------------------
\end{enumerate}

\end{document}
