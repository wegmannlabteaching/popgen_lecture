\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

\usepackage[withoutSolutions]{optional}


\DeclareMathOperator{\E}{\mathbb{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\\ {\color{red} \texttt{#1}}}}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}


%opening
\title{BL.0049 Population Genetics - Exercises 5}
\author{Prof. Daniel Wegmann}
\date{}


\begin{document}

\maketitle


\section*{Population Subdivision}
\begin{enumerate}[1.]
\item Two populations share migrants at a rate of $m=10^{-4}$ per generation per gene copy. Both populations have an effective size of $2N = 10^4$. \textit{(Adapted from exercises 4.4 from Nielsen \& Slatkin)}

\begin{enumerate}[a)]
\item What is the expected coalescent time between two gene copies sampled from the same population?
\sol{If two genes copies are sampled from the same population, the expect time until coalescence is given by $\E[t_S] = 4N = 2\cdot 10^4$.}

\item What is the expected coalescent time for two gene copies from different populations?
\sol{If two gene copies are sampled from different populations, the expected time until coalescence is $\E[t_D] = \frac{1}{2m} + 4N = \frac{1}{2\cdot 10^{-4}} + 2 \cdot 10^4 = 10^4(\frac{1}{2} + 2) = 2.5 \cdot 10^4$}

\item Assuming an infinite sites model, what is the expected value of $F_{ST}$ between these populations?
\sol{\\Under this model, $\E[H_S] = 2\E[t_S]\mu$ and $\E[H_T]=\frac{\E[t_S]+ \E[t_D]}{2}2\mu =  (\E[t_S] + \E[t_D])\mu$.\\ \\ Since $\E[F_{ST}] = \frac{\E[H_T] - \E[H_S]}{\E[H_T]} = \frac{(\E[t_S] + \E[t_D]) - 2\E[t_S]}{\E[t_S] + \E[t_D]} = \frac{\E[t_D] - \E[t_S]}{\E[t_S] + \E[t_D]}$.\\ \\ Using the values calculated above we get $\E[F_{ST}]  = \frac{2.5 \cdot 10 ^4 - 2\cdot 10^4}{2.5\cdot 10^4 + 2\cdot 10^4} = \frac{0.5}{4.5} = \frac{1}{9} = 0.11$ }
\medskip
\com{Of course one could also have simply use the already derived formula\\ \\ $\E[F_{ST}] = \frac{1}{1 + 16Nm}=\frac{1}{1+8 \cdot 10^4 \cdot 10^{-4}} = \frac{1}{1+8} = \frac{1}{9}$.
}

\end{enumerate}

%---------------------------------------------------------------------------------------------
\item Two populations diverged from each other $T=6 \cdot 10^3$ generations ago, and since then there has been no migration between the two populations. The effective size of both populations $2N_1 = 2N_2=10^4$. The effective size of the ancestral population from which the two populations diverged was also $2N_A = 10^4$. Assume that the mutation rate at a locus of length $l=10^5$ base pairs is $\mu=10^{-8}$ per base pair per generation and that the assumptions of the infinite sites model hold true. \textit{(Adapted from exercises 4.5, 4.6 \& 4.7 from Nielsen \& Slatkin)}

\begin{enumerate}[a)]
\item What is the expected number of nucleotide differences between two gene copies from population 1?
\sol{Since the ancestral population was of the same size as population 1, the expected genealogy of two genes sampled in population 1 is the same as for two genes sampled from a population of constant size. Hence, $\E[t_S] = 2N_1$, and therefor $E[\pi_1] = 2\E[t_S]\mu l=  2\cdot 10^4 \cdot 10^{-8} \cdot 10^5 =20$.}

\item What is the expected number of nucleotide differences between a gene copy from population 1 and one from population 2?
\sol{Since there is currently no migration between the populations, the two gene copies can only coalesce in the ancestral population. The expected time until their TMRCA is thus given by $\E[t_D] = T + 2N_A$. And hence the expected number of pairwise differences is\\ $\E[\pi_D] = 2(T + 2N_A)\mu l = 2(0.6\cdot 10^4 + 2\cdot 10^4)\cdot 10^{-8} \cdot^5 = 32$.}

\item What is the expected $F_{ST}$ between these populations?
\sol{\\ $\E[F_{ST}] = \frac{\E[H_T] - \E[H_S]}{\E[H_T]} = \frac{\E[t_D] - \E[t_s]}{\E[t_S] + \E[t_D]} = \frac{T+2N_A - 2N_1)}{T + 2N_A + 2N_1}$.\\ \\ Since $N_1=N_2=N_A$, $\E[F_{ST}] = \frac{T}{T + 4N_A} = \frac{6\cdot 10^3}{6\cdot 10^3 + 2 \cdot 10^4} = \frac{0.6 \cdot 10^4}{(0.6 + 2) \cdot 10^4} = \frac{0.6}{2.6} = \frac{3}{13}=0.23$.}
\medskip
\com{Alternatively one could just use the already derived formula\\ \\
$\E[F_{ST}] =  \displaystyle \frac{T}{T+4N} = \frac{6 \cdot 10^3}{6 \cdot 10^3 + 2\cdot 10^4} = 0.23$}

\item If the population size of the second population was larger, say $2N_2=10^5$, do you expect $F_{ST}$ to be larger or smaller?
\sol{If $2N_2$ was larger than as assumed for the calculations above, $F_{ST}$ would be smaller. The reason is that heterozygosity in the second population would be larger, and hence also $H_S$, which would lead to a smaller $F_{ST}$. An intuitive explanation may also be that larger populations experience less drift, and hence the population 2 would less different from the ancestral population, and in turn also less different from population 1.}

\end{enumerate}

\item Consider a sample of 60 individuals from each of two population. \textit{(Adapted from exercises 4.1 \& 4.6 from Nielsen \& Slatkin)}

\begin{tabular}{l c c c}
\hline
 & \textbf{AA} & \textbf{Aa} & \textbf{aa}\\
 \hline
 Population 1 & 20 & 20 & 20\\
 Population 2 & 15 & 15 & 30\\
 \hline
\end{tabular}


\begin{enumerate}[a)]
 \item Calculate the expected heterozygosity in each population.
\sol{First, let us calculate the allele frequencies in both population. We will do so for the arbitrary choice of allele A:\\ \\
$f_1 = \frac{2\cdot20 + 20}{120} = 0.5$, $f_2 = \frac{2\cdot15 + 15}{120} = 0.38$.\\\\
With these, we can now calulate the expected heterozygosity:\\
$H_{S1} = 2f_1(1-f_1) = 0.5$, $H_{S2} = 2f_2(1-f2) = 0.47$}

\item Calculate the expected heterozygosity of the total population.
\sol{Let us first calculate the allele frequency in the total population. This frequency is given by the average frequency of the two populations:\\ \\ $f_T =  \frac{f_1 + f_2}{2} = 0.44$,  and hence: $H_T = 2f_T(1-f_T) = 0.49$}

 \item Calculate $F_{ST}$ between these populations. Are they strongly diverged?
\sol{We need to calculate the expected heterozygosity within population $H_S = \frac{H_1 + H_2}{2} = 0.485$.\\ Then, $F_{ST} = \frac{H_T - H_S}{H_T} = \frac{0.49 - 0.485}{0.49} = 0.01$. \medskip \\ An $F_{ST}$ of 0.1 corresponds to moderate differentiation. This is substantially more than between any European population of humans, but about as much as between Europeans and Asians.}

\item Estimate the rate of migration per individual per generation between these populations, assuming that each population is of size $2N=10^4$.
\sol{Under such a model of symmetric migration, we expect $\E[F_{ST}] = \frac{1}{1 + 16Nm}$, and hence we can estimate the migration rate $\hat{m} = \frac{\frac{1}{F_{ST}} - 1}{16N} = \frac{1 - F_{ST}}{16N F_{ST}} = \frac{1 - 0.01}{8\cdot 10^4 \cdot 0.01} = 1.2 \cdot 10^{-3}$.}

\item Assume that these two populations diverged from a common ancestral population, that their sizes and the size of the ancestral population was was $2N=10^3$ and that there was no migration since the split. How many generations ago did these populations diverge?
\sol{Under a model of divergence, and if $N_1 = N_2 = N_A$, $\E[F_{ST}] = \frac{\E[t_S] - \E[t_D]}{\E[t_S] + \E[t_D]} = \frac{T}{T + 4N}$.\\ \\
We can thus get an estimate of the divergence by solving the above equation for $T$. We then get $\hat{T} = \frac{F_{ST}}{1-F_{ST}}4N = \frac{0.01}{0.99}2\cdot10^3 \frac{20}{0.99} = 20.2$ generations.}

\end{enumerate}

\end{enumerate}

\end{document}
