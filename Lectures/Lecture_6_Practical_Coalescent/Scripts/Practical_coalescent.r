#load libraries to conduct coalescent simulations (coala) and to plot genealogies (phytools)
#you can install these packages using install.packages("coala") and install.packages("phytools"), respectively.
library(coala)
library(phytools); 

#------------------------------------
# Functions to plot trees to a pdf
#------------------------------------
#Function to plot trees
plotTrees <- function(sumstats, sample_size, popCol=c("black", "orange2", "purple")){
  #convert trees to phylo opjects and identify tallest tree to adjust plotting
  nTrees <- length(sumstats);
  trees <- list(nTrees);
  height <- numeric(nTrees)
  for(i in 1:nTrees){
    trees[[i]] <- read.tree(text=sumstats[[i]]$trees[[1]]);
    height[i] <- max(branching.times(trees[[i]]));
  }
  
  #prepare plot
  width <- 8;
  pdf("genealogies.pdf", width=width, height=width/ 16 * 9)
  nTreesToPlot <- min(nTrees, 10);
  par(mfrow=c(1,nTreesToPlot), oma=c(0,4,0,0), las=1, xpd=NA)
  
  #plot all trees
  print(mean(height))
  maxHeight <- max(height);
  for(i in 1:nTreesToPlot){
    plotTree(trees[[i]], direction="downwards", ylim=c(0, maxHeight), lwd=0.6, ftype="off", cex=1, mar=c(0.1,0.7,0.1,0.7))
  
    #add tips and color by population
    popIndex <- rep(1:length(sample_size), times=sample_size);
    pop <- as.numeric(trees[[i]]$tip.label);
    
    nTips <- length(trees[[i]]$tip.label);
    symbols(1:nTips, rep(0, nTips), circles=rep(0.4, nTips), add=TRUE, inches=0.03, fg=NA, bg=popCol[popIndex[pop]])
  }
  
  #add axis
  axis(side=2, outer=T); 
  mtext("Time [4N]", side=2,adj=0.4, outer=T, line=3, cex=0.8, las=0);
  
  #close pdf
  dev.off();
};

#Function to plot SFS
plotSFS <- function(sumstats){
  #calculate average SFS across loci
  SFS <- sumstats[[1]]$sfs;
  if(length(sumstats) > 1){
    for(i in 2:length(sumstats)){
      SFS <- SFS + sumstats[[i]]$sfs;
    }
  }
  
  SFS <- SFS / sum(SFS);
  
  #calculate neutral expectation
  neutral <- 1/(1:length(SFS));
  neutral <- neutral / sum(neutral);
  
  #plot
  pdf("sfs.pdf", width=6, height=6);
  par(las=1, mar=c(4,4,0.1,0.1))
  plot(1:length(neutral), neutral, lty=2, col='blue', type='l', ylim=c(0, max(SFS, neutral)), xlab="Derived Allele Frequency", ylab="Frequency");
  points(SFS, type='b', pch=19);
  legend('topright', bty='n', lwd=1, lty=c(2,1), pch=c(NA, 19), col=c('blue', 'black'), legend=c("Neutral expectation", "Empirical"))
  dev.off();
}

#Function to simulate and plot
simulateAndPlot <- function(model, nsim=10){
  #add feature to model
  model <- model + sumstat_trees() + feat_mutation(1) + sumstat_nucleotide_div() + sumstat_sfs();
  
  #run simulations
  sumstats <- simulate(model, nsim=nsim);
  
  #plot trees
  plotTrees(sumstats=sumstats, sample_size=model$features[[1]]$get_sizes());
  
  #plot SFS
  plotSFS(sumstats);
  
  return(sumstats);
};


#----------------------------------------------------------------
# WF model: constant size
#----------------------------------------------------------------
model <- coal_model(sample_size=20, loci_number=100);
sumstats <- simulateAndPlot(model);

#----------------------------------------------------------------
# Exponential growth: expanding
#----------------------------------------------------------------
model <- coal_model(sample_size=20, loci_number=100) + feat_growth(5);
sumstats <- simulateAndPlot(model);

#----------------------------------------------------------------
# Exponential growth: shrinking for the past 2N generations
#----------------------------------------------------------------
model <- coal_model(sample_size=20, loci_number=100) + feat_growth(-1) + feat_growth(0, time=1);
sumstats <- simulateAndPlot(model);

#----------------------------------------------------------------
# Migration
#----------------------------------------------------------------
model <- coal_model(sample_size=c(10,10), loci_number=100) + feat_migration(0.25, symmetric=TRUE);
sumstats <- simulateAndPlot(model);

#----------------------------------------------------------------
# Split
#----------------------------------------------------------------
model <- coal_model(sample_size=c(10,10), loci_number=100) + feat_pop_merge(1, 2, 1);
sumstats <- simulateAndPlot(model);
