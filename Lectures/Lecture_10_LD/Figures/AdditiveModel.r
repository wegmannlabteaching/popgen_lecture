
f <- function(n, sigma){
  #get binom prob
  dens <- dbinom(0:n, n, prob=0.5)
  
  #calc means between 0 and 1
  means <- 0:n / n;
  
  #sum up densities between -1 and 2
  x <- seq(-1, 2, length.out=10000);
  y <- rep(0, length(x));
  yAtMean <- rep(0, length(means));
  
  for(i in 1:length(means)){
    y <- y + dens[i] * dnorm(x, mean=means[i], sd=sigma);
    yAtMean <- yAtMean + dens[i] * dnorm(means, mean=means[i], sd=sigma);
  }
  
  return(list(x=x, y=y, means=means, yAtMean=yAtMean, n=n));
}



#plot
genes <- c(1, 2, 5, 10);
sigma <- 0.035;
mycol <- '#ff5900'

pdf("additiveModel2.pdf", width=6, height=1.3)
par(mfrow=c(1,4), mar=c(0.1, 0.1, 0.1, 0.1), oma=c(1.4,0,1,0));
for(i in 1:length(genes)){
  dd <- f(2*genes[i], sigma);
  plot(dd$x, dd$y, type='l', ylim=c(0, max(dd$y * 1.2)), xlim=c(-0.1, 1.1), xaxt='n', yaxt='n', col=mycol, lwd=1)
  
  #add labels
  if(genes[i]<5){
    for(m in 0:dd$n){
      text(dd$means[m+1], dd$yAtMean[m+1], labels=paste0(c(rep("A", m), rep("a", dd$n-m)), collapse=""), pos=3, cex=0.9)
    }
  }
  
  if(genes[i] == 1){
    lab <- paste(genes[i], "gene");
  } else {
    lab <- paste(genes[i], "genes");
  }
  text(mean(par("usr")[1:2]), par("usr")[4] - 0.01*diff(par("usr")[3:4]), labels = lab, pos=3, font=2, xpd=NA);
}

mtext("Phenotype", side=1, adj=0.5, outer=TRUE, line=0.4, cex=0.7, font=2);
dev.off();
