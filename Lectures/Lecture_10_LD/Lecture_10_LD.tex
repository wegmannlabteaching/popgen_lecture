\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Linkage Disequilibrium (LD)}

%TOPIC Linkage Disequilibrium
%TOPIC Association mapping
%READING Chapter 6 (pages 107 - 126)
%EXERCISES Exercises_6

\begin{document}
\maketitle

%TODO Add GWAS (already in medical genetics...) and discussion of recombination maps!
%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}

 
 \frametitle{Recap}
 
  \begin{itemize} 
     \item The basis of most inference techniques is the \textbf{Likelihood function}.
     \item We have seen two inference techniques: \textbf{Maximum Likelihood} and \textbf{Bayesian Estimation}
     \item These techniques can be used to assign individuals to populations or to infer evolutionary trees.
     
     \item Inferred trees are \emph{coalescent trees}, which may differ from population or species trees due to \textbf{incomplete lineage sorting}.
     
     \item Evolutionary histories should thus be inferred from multiple / many loci.
         
  \end{itemize}
  
  \pause
 \textbf{Question:} How independent are the loci in a genome?\\\bigskip
  
  
\end{frame}

 %------------------------------------------------------------------------
 \section{Linkage Disequilibirum}
\begin{frame}
 \frametitle{Glucose-6-phosphate dehydrogenase (G6PD) Deficiency}
 
  G6PD is an enzyme of the Pentose Phostphate Pathway that is providing NADPH for the reduction of Glutathion, which is neutralizing free radicals.\\\bigskip
  
  \makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{Figures/Ox_Pentose_phosphate_pathway.pdf}}%

  \pause
 About 400 million people suffer from G6PD deficiency world-wide, mostly people of African, Middle Eastern or South Asian ancestry.\\\medskip
 When exposed to elevated oxidative stress, for instance after eating specific foods (e.g. Fava beans), these people suffer from hemolytic anemia.\\\medskip
 As a side effect, this deficiency confers protection against malaria.
 
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Glucose-6-phosphate dehydrogenase (G6PD) Deficiency}
  \textbf{Single Nucleotide Variants (SNVs) in and around G6PD}\\
 \includegraphics[width=\textwidth]{Figures/LD_G6PD.png}
 
 \pause
 \textbf{Note:} SNVs are not independent, they are in linkage disequilibrium (LD)!
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Measuring Linkage Disequilibrium}
 
 
 \textbf{Haplotype}\\
 A specific combination of alleles on the same chromosome. In case of two loci with alleles A/a and B/b, the possible haplotypes are $AB$, $Ab$, $aB$ and $ab$.\\\bigskip
 
 \textbf{Expected haplotype frequencies under independence}\\
 When two loci are segregating independently, we expect a fraction $\E[f_{AB}] = f_Af_B$ of all haplotypes to be $AB$.\\\bigskip
 
 \textbf{Coefficient of LD $D$}\\
 The coefficient $D_{AB} = f_{AB} - f_Af_B$ is measuring how different the haplotype frequencies are form their expected values under independence.\\\medskip
 
 If $D_{AB} = 0$, the alleles $A$ and $B$ are said to be in linkage equilibrium.\\\bigskip
 
 \pause
 \textbf{Note:} each pair of alleles has its own $D$!
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Measuring Linkage Disequilibrium: Example}
 
 \begin{columns}[T]
	\begin{column}{5cm}
		\textbf{Observed Haplotypes}\\  
		\begin{tabular}{c c c c}
		\hline
		$n_{AB}$ & $n_{Ab}$ & $n_{aB}$ & $n_{ab}$ \\
		\hline
		50 & 20 & 10 & 20\\  
		\hline  
		\end{tabular}
	\end{column}
	\begin{column}{5cm}
	\pause
		\textbf{Haplotype Frequencies}\\  
		\begin{tabular}{c c c c c c c c}
		\hline
		$f_{AB}$ & $f_{Ab}$ & $f_{aB}$ & $f_{ab}$\\
		\hline
		0.5 & 0.2 & 0.1 & 0.2\\  
		\hline  
	      \end{tabular}
	\end{column}
\end{columns}
 
 
 \bigskip
 \pause
 \textbf{Allele Frequencies}\\  
  \begin{tabular}{c c c c}
  \hline
 $f_A$ & $f_a$ & $f_B$ & $f_b$\\
  \hline
  $f_{AB} + f_{Ab} = 0.7$ & $f_{aB} + f_{ab} = 0.3$ & $f_{AB} + f_{aB} = 0.6$ & $f_{Ab} + f_{ab} = 0.4$\\  
  \hline  
 \end{tabular}

 
 \bigskip
 \pause
 \textbf{Coefficients of LD}\\  
   \begin{tabular}{c c c c}
  \hline
 $D_{AB}$ & $D_{Ab}$ & $D_{aB}$ & $D_{ab}$\\
  \hline
  $f_{AB} - f_Af_B = 0.08$ & $f_{Ab} - f_Af_b = -0.08$ & $f_{aB} - f_af_B = -0.08$ & $f_{ab} - f_af_b = 0.08$\\
  \hline  
 \end{tabular}
 
 \bigskip\bigskip
 \pause
 \textbf{Note:} in case of two alleles per locus, there are simple relationships among the different $D$ values!
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Measuring Linkage Disequilibrium}
 
 \textbf{Relating individual $D$}\\
 If there are only two alleles per locus, we have:\\
 
 \begin{align*}
  D_{AB} &= f_{AB} - f_Af_B\\
  &= f_A - f_{Ab} -f_Af_B \mbox{\ \ \ since \ \ \ } f_A = f_{AB} + f_{Ab} \rightarrow f_{AB} = f_A - f_{Ab}\\
  &= f_A(1-f_B) - f_{Ab}\\
  &= f_Af_b - f_{Ab} = -D_{Ab}
 \end{align*}

 \bigskip
  Similarly, we get $D_{AB} = D_{ab}$ and $D_{Ab} = D_{aB}$.\\\medskip
  
  Hence, a single value of D can characterize LD perfectly:
  
  \begin{equation*}
  D = D_{AB} = -D_{Ab} = -D_{aB} = D_{ab}
  \end{equation*}
  
  

  

 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Measuring Linkage Disequilibrium}
 
 \textbf{The problem of scale}\\
 When is $D$ large, when is it small? To answer this question, we have to relate $D$ to the maximally possible $D$ given the allele frequencies.
 
  
 \begin{columns}[T]
	\begin{column}{5cm}
	Since ...\vspace{-0.4cm}
	\begin{align*}
	 D_{AB} &= f_{AB} - f_Af_B = D\\
	 D_{Ab} &= f_{Ab} - f_Af_b = -D\\
	 D_{aB} &= f_{aB} - f_af_B = -D\\
	 D_{ab} &= f_{ab} - f_af_b = D\\
	\end{align*}

	\end{column}
	\begin{column}{5cm}
	... we have\vspace{-0.45cm}
	\begin{align*}
	 f_{AB} = f_Af_B + D\\
	 f_{Ab} = f_Af_b - D\\
	 f_{aB} = f_af_B - D\\
	 f_{ab} = f_af_b + D\\	 
	\end{align*}
	\end{column}
\end{columns}
 
 \pause
 
 From this we see that \vspace{-0.25cm}
 \begin{eqnarray*}
  |D| \leq \mbox{min}(f_Af_b, f_af_B) \quad \mbox{if} \quad D>0\\
  |D| \leq \mbox{min}(f_Af_B, f_af_b) \quad \mbox{if} \quad D<0
 \end{eqnarray*}\vspace{-0.5cm}
 \pause
 
 $D$ is at its maximal value if at least one haplotype is missing from the sample.
 
 For instance, if $f_{Ab} = 0$, in which case $D_{Ab} = f_{Ab} - f_Af_b = - f_Af_b$. 
 
\pause
\smallskip
\textbf{Note:} this implies that in case of a new mutation, $D$ is maximized!
 
 
\end{frame}

% %------------------------------------------------------------------------
% \begin{frame}
%  \frametitle{Measuring Linkage Disequilibrium}
%  
%  \textbf{Defining $D'$}\\
%  The coefficient of LD $D'$ related $D$ to its maximal value:
%  
%  \begin{equation*}
% \begin{array}{l}
% D' = \left\{ \begin{array}{ll}
% 			   \displaystyle\frac{D}{\mbox{min}(f_Af_b, f_af_B)} \quad &\mbox{if $D>0$}\medskip \\
%                            \displaystyle\frac{-D}{\mbox{min}(f_Af_B, f_af_b)} \quad &\mbox{if $D<0$}\medskip \\                           
%                            \end{array}                           
% \right. 
%  \end{array}
% \end{equation*}
%  
%  \pause
%  
%  $D'$ is guaranteed to be between 0 and 1.\\
%  If $D' = 1$, then $D$ is at its maximal value, which implies that at least one haplotype is missing from the sample.\\\medskip
%  
%  For instance, $D'=1$ if $f_{Ab} = 0$, in which case $D_{Ab} = f_{Ab} - f_Af_b = - f_Af_b$. 
%  
% \pause
% \bigskip
% \textbf{Note:} this implies that in case of a new mutation, $D'=1$ as one haplotype is missing!
%  
% \end{frame}

%------------------------------------------------------------------------
% Evolution of D
\section{Evolution of D}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Recombination breaks haplotypes apart}
 
 \includegraphics[width=0.9\textwidth]{Figures/Crossingover.jpg}
 
 %TODO: better picture? Draw yourself!
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
  \frametitle{Recombination breaks haplotypes apart}
  
 \textbf{Recombination rate $c$}\\
 The recombination rate $c$ is the fraction of recombinant gametes produced by a double heterozygote (e.g. $AaBb$).\\\medskip
 
 If two loci are independent, a double heterozygote (e.g. $AaBb$) produces these four gametes in equal proportions (according to Mendel): $AB$, $Ab$, $aB$ and $ab$.\\\medskip
 
 
 \textbf{Hence:} $0 \leq c \leq \frac{1}{2}$, where $c=\frac{1}{2}$ indicates independence.
 
  
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Evolution of $D$}
 
 \textbf{Change in $f_{AB}$ in one generation} \\
 $AB$ gametes are produced by the following individuals:\\\medskip
 
 \begin{tabular}{c c c}
  \hline
  Genotype & Frequency & Fraction of $AB$ gametes\\
  \hline
  $AB/AB$ & ${f_{AB}}^2$ & 1\\
  $AB/Ab$ & $2f_{AB}f_{Ab}$ & $\frac{1}{2}$\\
  $AB/aB$ & $2f_{AB}f_{aB}$ & $\frac{1}{2}$\\
  $AB/ab$ & $2f_{AB}f_{ab}$ & $\frac{1-c}{2}$\\
  $Ab/aB$ & $2f_{Ab}f_{aB}$ & $\frac{c}{2}$\\  
  \hline
 \end{tabular}

 \pause
 \begin{align*}
  \E[f_{AB}(t+1)] &= {f_{AB}}^2 + \frac{2f_{AB}f_{Ab}}{2} + \frac{2f_{AB}f_{aB}}{2} + \frac{2f_{AB}f_{ab}(1-c)}{2} + \frac{2f_{Ab}f_{aB}c}{2}\\
  &= {f_{AB}}^2 + f_{AB}f_{Ab} + f_{AB}f_{aB} + f_{AB}f_{ab} - c(f_{AB}f_{ab} - f_{Ab}f_{aB})\\
  &= f_{AB} ( f_{AB} + f_{Ab} + f_{aB} + f_{ab} ) - c(f_{AB}f_{ab} - f_{Ab}f_{aB})\\
  &= f_{AB} - c(f_{AB}f_{ab} - f_{Ab}f_{aB})
 \end{align*}
 \pause
 Hence, $\E[f_{AB}(t+1)]$ is equal to $f_{AB}$ minus those broken up by recombination plus those created by recombination.
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Evolution of $D$}
 
 \textbf{Change in $D$ in one generation}\\
 So far we have $\E[f_{AB}(t+1)] = f_{AB} - c(f_{AB}f_{ab} - f_{Ab}f_{aB})$.\\\medskip
 
 
 
 Using the fact that $f_{AB} = f_Af_B + D$, $f_{Ab} = f_Af_b - D$, $f_{aB} = f_af_B - D$ and $f_{ab} = f_af_b + D$, we then get:
 \begin{align*}
 \E[f_Af_B(t+1)] + \E[D(t+1)] &= f_Af_B + D - c(f_{AB}f_{ab} - f_{Ab}f_{aB})\\
 \E[D(t+1)] &= D - c(f_{AB}f_{ab} - f_{Ab}f_{aB}) \quad  \mbox{since } \E[f_Af_B(t+1)] = f_Af_B
 \end{align*}

 And further:
 
\begin{align*}
 f_{AB}f_{ab} - f_{Ab}f_{aB} &= (f_Af_B + D)(f_af_b + D) - (f_Af_b - D)(f_af_B - D)\\
 &= f_Af_Bf_af_b + D(f_Af_B + f_af_b) + D^2 - f_Af_Bf_af_b + D(f_Af_b + f_af_B) - D^2\\
 &= D(f_Af_B + f_af_b + f_Af_b + f_af_B) = D
\end{align*}

\bigskip
We thus get finally: $\E[D(t+1)] = D - cD = (1-c)D$
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Evolution of $D$}
 
 \textbf{Evolution of $D$}\\
 We just showed that $\E[D(t+1)] = (1-c)D$.\\\medskip
 
 The time to reach linkage equilibrium depends on the recombination rate $c$.\\\medskip
 
 Applying our result repeatedly, we have:  $\E[D(t)] = (1-c)^tD(0)$

 To reduce $D$ to $10\%$ of its initial value, it thus takes
 
 \begin{equation*}
  \frac{1}{10}D(0) = (1-c)^tD(0) \quad \rightarrow \quad -\log 10 = t \log (1-c) \quad \rightarrow \quad t = \frac{-\log 10}{\log (1-c)}
 \end{equation*}

 \pause
 In humans, $c \approx 0.01$ for two loci 1 Mb ($10^6$ base pairs) apart.\\
 For two loci 1Mb apart, it thus takes about 300 generation to reduce $D$ to 10\% of its original value.\\
 For two loci in neighboring genes ($\approx \frac{1}{7}$ Mb apart), it takes $>1600$ generations or $> 40,000$ years!
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Two-Locus Wahlund Effect}
  \textbf{Two-Locus Wahlund effect}\\
 The increase in linkage-disequilibrium (LD) due to population subdivision.\\\bigskip
 
 \textbf{Example: two loci in two populations}\\
 Consider two loci with alleles $A/a$ and $B/b$ on different chromosomes and in linkage equilibrium in each population ($D=0$).\\\medskip
 \pause
 Allele frequencies are denoted by $f_A^{(1)}$, $f_B^{(1)}$, $f_A^{(2)}$ and $f_B^{(2)}$. In a mixed sample from both populations: 
  \begin{align*}
 f_{AB} &= \frac{f_A^{(1)}f_B^{(1)} + f_A^{(2)}f_B^{(2)}}{2} \quad \quad f_A = \frac{f_A^{(1)}+f_A^{(2)}}{2} \quad \quad f_B = \frac{f_B^{(1)}+f_B^{(2)}}{2} \\ 
  D &= f_{AB} - f_Af_B = \frac{f_A^{(1)}f_B^{(1)} + f_A^{(2)}f_B^{(2)}}{2} - \frac{f_A^{(1)}+f_A^{(2)}}{2} \frac{f_B^{(1)}+f_B^{(2)}}{2}\\
  &= \frac{(f_A^{(1)} - f_A^{(2)})(f_B^{(1)} - f_B^{(2)})}{4}
 \end{align*}

 \pause
 \textbf{Note:} since $\E[D(t+1)] = (1-c)D$, the two-locus Wahlund effect does \emph{not} disappear after one generation of random mating (it is just halfed)!
  
\end{frame}

%------------------------------------------------------------------------
\section{Genealogical interpretation}
%------------------------------------------------------------------------
\begin{frame}[t]
 \frametitle{Genealogical interpretation of LD}
 
 \begin{columns}[T]
	\begin{column}{5cm}
	  \textbf{Recombination splits lineages}\\
	  Going backward in time, a recombination event splits a lineage into two independent lineage, each with a part of the locus.\\\medskip
	  
	  
	  \textbf{Coalescenes merges lineages}\\
	  In contrast, coalescent events merge lineages together.\\\medskip
	   
	  
	\end{column}
	\begin{column}{5cm}
	  \includegraphics[width=\columnwidth]{Figures/Ancestral_recombination_graph.pdf}
	\end{column}
\end{columns}
 
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}[t]
 \frametitle{Genealogical interpretation of LD}
 
 \begin{columns}[T]
	\begin{column}{5cm}
	  \textbf{Probability that two loci have the same genealogy}\\
	  
	  Per generation, we have:
	  $\P$(coalescent event) $=\frac{1}{2N}$\\\medskip
	  
	  $\P$(recombination event) $=2c$.\\\medskip
	  
	  $\P$(coalescencne before recombination) \\\medskip
	  
	  $=\displaystyle \frac{\frac{1}{2N}}{\frac{1}{2N} + 2c} = \displaystyle\frac{1}{1+4Nc}$
	  
	  
	  \pause
	  \medskip
	  \textbf{Example}\\
	 If $N=10^6$ and $c=0.01$ per Mb, the probability that two loci that are 1Kb apart have the same tree is thus
	  
	  $\frac{1}{1+4\cdot10^6 \cdot10^{-5}} = \frac{1}{1 + 4\cdot10} \approx 2.4\%$
	  	   
	  
	\end{column}
	\begin{column}{5cm}
	  \onslide<1->\includegraphics[width=\columnwidth]{Figures/Ancestral_recombination_graph.pdf}
	\end{column}
\end{columns}
 
 
\end{frame}

%------------------------------------------------------------------------
\section{Association Studies}
%------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Complex Traits}

\begin{columns}[T]
	\begin{column}{5.2cm}

	\textbf{Inheritance}\\
    Offspring ressemble their parents.\\\medskip
    In the 19th century, this was explained by \textbf{Blending inheritance}.\\
    \end{column}	
    \begin{column}{5cm}
     \onslide<2->{\textbf{Complex traits}\\
    Many traits are continous and seemingly ``blending in''.\\\medskip        
    Such traits are called \textbf{complex traits}.}
    \end{column}
\end{columns}

\bigskip\medskip

\begin{columns}[T]
	\begin{column}{5.2cm}
	\vspace{0.2cm}
	 \includegraphics<1->[width=\columnwidth]{Figures/blending.jpg}     
	\end{column}
	\begin{column}{5cm}	
	 \includegraphics<2->[width=0.95\columnwidth]{Figures/parent_offspring_height.png}
	\end{column}
\end{columns}

\end{frame}
%------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Complex traits}

 \begin{columns}[T]
 \begin{column}{5cm}
 
 
  \textbf{Affected by the environment.}\\
  Non-genic effects on complex traits.
  \medskip
  
  \includegraphics[width=\columnwidth]{Figures/heightOverTime.png}
  
  
  
 \end{column}
 
 \begin{column}{5.6cm}
    
    \textbf{Polygenic inheritance}\\
    Many genes affect complex traits.\\
  
  \medskip
  
  \includegraphics<2->[width=\columnwidth]{Figures/additive_height.jpg}  
 \end{column}
\end{columns}


\bigskip 
\includegraphics<2->[width=\columnwidth]{Figures/additiveModel2.pdf}


\end{frame}

%------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Finding the genetic basis of heritable traits}

\begin{columns}[T]
 \begin{column}{7cm}
\textbf{Genome-wide Association Studies (GWAS)}\\
\begin{enumerate}
 \item Measure the trait in many individuals.
 \item Genotype these individuals.
 \item Test for an association between genotypes and phenotypes.
\end{enumerate}

\vspace{1.1cm} 

\includegraphics<2->[width=\textwidth]{Figures/manhatten_plot.png}

 \end{column}
 \begin{column}{4.2cm}
\includegraphics<1->[width=\textwidth]{Figures/association_regression.pdf}  
 \end{column}
 \end{columns}

\end{frame}

%------------------------------------------------------------------------

\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize} 
       
    
    \item Loci on the same chromosome are linked, and so is there history.
    
    \item A measure of non-idependence of linked loci (the \textbf{Linkage-Disequilibrium}) is $D$, defined for each pair of alleles.
    \item In the case of two alleles per locus, a single value of $D$ is enough to characterize LD.
    \item $D$ evolves as a function of the recombination rate $c$.
    \item Importantly, one generation is \emph{not} enough to reach linkage-equilibrium, even if the loci are on different chromosomes!
    \item The presence of LD allows us to identify variants affecting complex traits.
    
 \end{itemize}
 
 \pause
 \begin{block}{}
  For next week: please solve \textbf{Exercises 7} and read \textbf{Chapter 7}.
 \end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
