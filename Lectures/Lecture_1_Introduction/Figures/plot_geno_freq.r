a <- read.table("/home/wegmannd/Dropbox/Teaching/Population Genetics/Lecture 1 - Introduction/EEB135_example.randomized.txt", header=F)

a <- a[-which(a[,5]>0.7),]

for(i in 1:2){
pdf(paste("/home/wegmannd/Dropbox/Teaching/Population Genetics/Lecture 1 - Introduction/GenoVsFreq_", i, ".pdf", sep=""), width=5, height=4.5)

par(mar=c(4,4,0.1,0.1))

plot(a[,7], a[,4], xlim=c(0,1), ylim=c(0,1), col='red', pch=21, xlab="Frequency of reference allale p = f(R)", ylab="Genotype frequencies")
points(a[,7], a[,5], xlim=c(0,1), ylim=c(0,1), col='purple', pch=21)
points(a[,7], a[,6], xlim=c(0,1), ylim=c(0,1), col='blue', pch=21)

#add model
if(i>1){
  print("YES!!!!");
  s <- seq(0,1, length.out=100)
  lines(s, s^2, col='black', lwd=5);
  lines(s, 2*s*(1-s), col='black', lwd=5);
  lines(s, (1-s)^2, col='black', lwd=5);
}

legend('top', legend=c("RR", "RA", "AA"), col=c("red", "purple", "blue"), pch=19, cex=1.2, bty='n');
dev.off()
}