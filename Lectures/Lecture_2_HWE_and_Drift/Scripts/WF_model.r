N <- 10^3; f_init <- 0.5;
len <- 1000;
rep <- 10;

#open plot
plot(0, type='n', xlim=c(1,len), ylim=c(0,1), xlab="Generations", ylab="Allele frequency")
col <- c("red", "blue", rep("gray", rep-2));

#run replicates
f <- numeric(len);
f[1] <- f_init;
for(i in 1:rep){  
  for(g in 2:len){
    f[g] <- rbinom(1, size=N, prob=f[g-1])/N;
  }  
  lines(1:len, f, col=col[i], lwd=2);
}


