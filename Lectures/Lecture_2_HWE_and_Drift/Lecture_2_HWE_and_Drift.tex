\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{HWE \& Genetic Drift}

%TOPIC Deviations from the Hardy-Weinberg Equilibrium.
%TOPIC Testing for such deviations.
%TOPIC Introduction to genetic drift / the Wright-Fisher Model.
%READING Chapter 1 (pages 12 - 19)
%READING Chapter 2 (pages 21 - 27)

%TODO: Add simple exercise to discuss if there is too much time!}

\begin{document}
\maketitle
%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Hardy-Weinberg Equilibrium}

\textbf{Implications of HWE}
\begin{enumerate}
\item Offers a general rule explaining how genotype frequencies and allele frequencies are related.
\item The expected genotype frequencies in a randomly mating population with allele frequencies $p$ and $q=1-p$ are $X=p^2$, $Y=2pq$ and $Z=q^2$.
\item No matter what X, Y, and Z are, the genotype frequencies will fall into the HWE proportions after a single generation of random mating.\begin{itemize}                                                                                                                                       \item In the presence of random mating alone, the HWE will persist over time.
\item This partially explains its pervasiveness.
\end{itemize}
\item Allows us to work and think almost exclusively in terms of allele frequencies.
\item Under HWE we expect allele frequencies not to change ($p'=p$)!
\end{enumerate}


\end{frame}
%-----------------------------------------------------------------------------------------
\section{Deviations from HWE}
\begin{frame}
\frametitle{Deviations from HWE}

\bigskip
\textbf{Assortative mating}\\
If individuals prefer to mate with individuals of the same genotype, a deficite of heterozygous individuals will be observed.

\pause
\bigskip
\begin{columns}[T]
 \begin{column}{5cm}
\textbf{Selection}\\
If individuals of a specific genotype are less likely to survive, the genotype frequencies will deviate from HWE.
 \end{column}
 \begin{column}{5cm}
  \includegraphics[width=0.5\columnwidth]{Figures/selection_HWE.pdf}
 \end{column}
\end{columns}


\pause
\bigskip
\bigskip
\begin{columns}[T]
 \begin{column}{5cm}
  \textbf{Population structure}\\
If samples are drawn from different populations with different allele frequencies, deviations are expected even if each population is in HWE.
 \end{column}
 \begin{column}{5cm}
  \includegraphics[width=\columnwidth]{Figures/popstructure_HWE.pdf}
 \end{column}
\end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Deviations from HWE}

\textbf{Inbreeding}\\
Occurs if individuals mate close relatives more often than expected by chance.\\

Related individuals have likely similar genotypes $\Rightarrow$ reduction in heterozygots.\\\medskip

\pause
\textbf{Measuring deviations from HWE}\\
Deviations are measured by the \emph{inbreeding coefficient} $F$ - the factor by which the number of heterozygots is reduced compared to HWE.

\begin{equation*}
 f_{Aa} = 2f_Af_a(1-F) \ \ \Rightarrow \ \ F = \frac{2f_Af_a - f_{Aa}}{2f_Af_a}
\end{equation*}
\pause
\textbf{Estimating $F$}\\
Consider a locus with $f_{AA}=0.58$, $f_{Aa}=0.07$ and $f_{aa}=0.35$.\\\medskip
\pause
Allele frequencies: $\displaystyle f_A = f_{AA} + \frac{f_{Aa}}{2} = 0.58 + \frac{0.07}{2} = 0.62$ and $\displaystyle f_a = 1-f_A = 0.38$\\\medskip
\pause
Inbreeding coefficient: $\displaystyle F = \frac{2f_Af_a - f_{Aa}}{2f_Af_a} = \frac{2\cdot 0.62\cdot 0.38 - 0.07}{2\cdot 0.62\cdot 0.38} = 0.85$\\\medskip

Hence, there is considerable inbreeding in this population!\\(Data is from the selfing wild oat \textit{Avena fatua}).
\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Deviations from HWE}

\textbf{Random deviations from HWE}\\
Samples may deviate from HWE by chance, even if the population is in HWE.\\
\textbf{Example:} $~32$\% of all samples of 4 individuals from a population with $f_A=0.5$ show no AA genotype!\\\medskip
\pause
We thus need a statistical test to say that a population is not in HWE.

\pause
\bigskip
\textbf{Hypothesis Testing}\\
In Hypothesis testing we calculate the \textbf{$\boldsymbol{p}$-value}: the probability to obtain data such as the observed data or even more extreme under a Null hypothesis ($H_0$).\\\medskip
If $p < \alpha$ we reject $H_0$ (by convention we use $\alpha=0.05$).

\pause
\bigskip
\textbf{What is a statistical test?}\\
An easy way to estimate $p$: instead of $p$, we calculate a test statistics (e.g. $\chi^2$) and compare it to a pre-calculated table.

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Testing for deviations from HWE}

\textbf{The $\chi^2$ test}\\
This test is used to compare observed categorical data to their expectations.\\
\textbf{Example:} observed genotype counts to those expected under HWE.\\\medskip
The test statistics $\chi^2$ is a distance between observed $O$ and expected $E$ counts:
\begin{equation*}
 \chi^2 = \sum_{i=1}^k\frac{(E_i - O_i)^2}{E_i}
\end{equation*}

\pause
\bigskip

 \textbf{Example: Sickle cell disease}\\
In some  populations, two hemoglobin alleles are segregating: $Hb^A$ and $Hb^S$.\\
In a study, the following genotypes were observed among 500 children:
%Note: this is fake data: find real data!
\begin{columns}[T]
 \begin{column}{6cm}\begin{block}{}
\begin{tabular}{c c c c}
 \hline
 $Hb^AHb^A$ & $Hb^AHb^S$ & $Hb^SHb^S$ & Total\\
 \hline
 145 & 321 & 34 & 500\\
 \hline
\end{tabular}
\end{block}
 \end{column}
 \begin{column}{4cm}
 \vspace{0.75cm}
  \textbf{Question:} are they in HWE?
 \end{column}
\end{columns}
\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Testing for deviations from HWE}
\begin{columns}[T]
 \begin{column}{6cm}
 \textbf{Observed genotype counts}\\\medskip
\begin{tabular}{c c c c}
 \hline
 $Hb^AHb^A$ & $Hb^AHb^S$ & $Hb^SHb^S$ & Total\\
 \hline
 145 & 321 & 34 & 500\\
 \hline
\end{tabular}
 \end{column}
\begin{column}{4cm}
\pause
 \textbf{Allele frequencies}\pause
\begin{align*}
f_A &= \displaystyle \frac{2*145+321}{2*500} &= 0.61\\
f_S &= 1-f_A &= 0.39
\end{align*}
\end{column}
\end{columns}

\pause
\bigskip
\begin{columns}[T]
 \begin{column}{7cm}
 \textbf{Expected genotype counts}\\\medskip\pause
%\begin{block}{}
\begin{tabular}{c c c c}
 \hline
 $Hb^AHb^A$ & $Hb^AHb^S$ & $Hb^SHb^S$\\
 \hline
 $f_Af_A \cdot 500$ & $2 f_A f_S \cdot 500$ & $f_Sf_S\cdot 500$\\
 186 & 238 & 76\\
 \hline
\end{tabular}
 \end{column}
\begin{column}{3cm}
 \end{column}
\end{columns}

\pause
\bigskip
\bigskip
\begin{columns}[T]
 \begin{column}{7cm}
\textbf{Calculating $\chi^2$}\pause
 \begin{align*}
 \chi^2 &= \displaystyle \frac{(186-145)^2}{186} + \frac{(238-321)^2}{238} + \frac{(76-34)^2}{76}\\ &= 61.19
\end{align*}
\pause
\vspace{-0.35cm}
\begin{alertblock}{}
 HWE is rejected with $p<0.001$
\end{alertblock}
 \end{column}
\begin{column}{3cm}
%\medskip
\textbf{$\chi^2$ table}\\\medskip
\begin{tabular}{c c}
\hline
$\chi^2$ & $p$\\
\hline
2.71 & 0.10\\
3.84 & 0.05\\
6.64 & 0.01\\
10.83 & 0.001\\
\hline
\end{tabular}
 \end{column}
\end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Sickle Cell Disease and Malaria}

 \begin{columns}[T]
  \begin{column}{5cm}
  \includegraphics[width=\columnwidth]{Figures/b7_sickle-cell.jpeg}
 \end{column}
\begin{column}{5cm}
  \includegraphics[width=\columnwidth]{Figures/HBS_map_modif.jpg}
  \end{column}
 \end{columns}

\end{frame}


%-----------------------------------------------------------------------------------------
\section{Genetic Drift}
\begin{frame}
\frametitle{Allele frequencies DO change over time!}

\begin{columns}[T]
\begin{column}{4.5cm}
\textbf{Experiment with \textit{D. melanogaster}}\\
In a classic experiment, 107 populations of 16 individuals of \textit{D. melanogaster} where kept in veils for 19 generations.\\\medskip

After reproduction, 8 males and 8 females where randomly picked to form the next generation.

\end{column}
\begin{column}{5.5cm}
 \includegraphics[width=\columnwidth]{Figures/GeneticDrift_DMelanogaster_experiment.png}
\end{column}
\end{columns}
\bigskip
\pause
\begin{alertblock}{}
 In contrast to the HWE result, allele frequencies do change!
\end{alertblock}

\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{HWE is an expectation}

\textbf{The expectation of a random process}\\
The expectation of a random process is the average outcome if we were to replicate the process infinitely many times.\\\medskip
\textbf{Example:} if $f_A=0.5$, how many heterozygots do we expect among 1 individual? Answer: $2f_Af_a=0.5$.\\\medskip
\pause
Hence we sometimes see 1, sometimes 0, but \emph{on average} 0.5

\pause
\bigskip
\textbf{HWE is an expectation}\\
Under HWE we \emph{expect} the allele frequency not to change, but in reality we will see random fluctuations.

\pause
\bigskip

\begin{alertblock}{Definition: Genetic Drift}
 A change in allele frequency because alleles {\em by chance} produce more / less offspring in any given generation.
\end{alertblock}

\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Models of Genetic Drift}

 \includegraphics[width=\textwidth]{Figures/Population_models.pdf}

  \begin{columns}[T]
   \begin{column}{3.2cm}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true,shadow=true]{block body}
	 \setstretch{1}{\small Individuals are drawn randomly with replacement from the previous generation.}
       \end{beamercolorbox}
   \end{column}
   \begin{column}{3.2cm}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true,shadow=true]{block body}
	 \setstretch{1}{\small Every timestep, one individual is chosen randomly to reproduce and one individual to die.}
       \end{beamercolorbox}
   \end{column}
   \begin{column}{3.2cm}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true,shadow=true]{block body}
	 \setstretch{1}{\small Every generation, each individual has zero, one, or two descendants.}
       \end{beamercolorbox}
   \end{column}
  \end{columns}

 \bigskip
 \pause
 While these models turn out to be very similar mathematically, we will focus on the \textbf{Wright-Fisher} model, as it is the easiest and most well studied.


\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{The WF-model}
\textbf{Basic assumption: random picking of gametes}\\
The WF-model assumes that $N_t$ adults produce a large number of gametes, of which $N_t+1$ are chosen randomly to constitute the next generation $t+1$.

\bigskip
\includegraphics[width=\textwidth]{Figures/GeneticDrift_01.png}

\pause
\bigskip
\begin{alertblock}{Definition Genetic Drift}
 {\em Genetic drift}: a change in allele frequency because alleles {\em by chance} produce more / less offspring in any given generation.
\end{alertblock}
\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{The WF-model}

\textbf{Simulating the WF-model}\\
Since the WF-model only assumes that gametes (and hence parents) are picked at random, it is super simple to simulate.\\\medskip

But drawing a random number for each individual is rather slow...\\\bigskip
\pause
\textbf{Transition probabilities}\\
If $i$ out of $N_t$ adults carry allele \alert{$a$}, and thus a proportion $p=\frac{i}{N}$ of all gametes will be of type \alert{$a$}, then

$$P(X_{t+1}=j|X_{t}=i)={N\choose j} p^j(1-p)^{N-j}$$ where

${N\choose j}=$  all ways you can arrange $N$ gametes to have $j$ gametes of type \alert{$a$}.

\bigskip
Note: this distribution is known as the {\em binomial sampling distribution}.


\end{frame}
%-----------------------------------------------------------------------------------------

\begin{frame}
\frametitle{The WF-model: theory meets data}
\begin{columns}
\begin{column}{5cm}
 \includegraphics[width=\columnwidth]{Figures/GeneticDrift_DMelanogaster_experiment.png}
\pause
\end{column}
\begin{column}{5cm}
\includegraphics[width=\columnwidth]{Figures/GeneticDrift_DMelanogaster_theory.png}
\end{column}
\end{columns}
\end{frame}

%-----------------------------------------------------------------------------------------

\begin{frame}
\frametitle{Simulating the WF-model}

\begin{codeblock}{Example R code}{\small
N $<-$ 1000; f\_init $<-$ 0.5;\\
len $<-$ 1000\\
rep $<-$ 5;\\\medskip
\#open plot\\
plot(0, type='n', xlim=c(1,len), ylim=c(0,1), xlab="Generations", ylab="Allele frequency");\\
col $<-$ c("red", "blue", rep("gray", rep-2));\\\medskip
\#run replicates\\
f $<-$ numeric(len); f[1] $<-$ f\_init;\\
for(i in 1:rep)\{\\
\hspace{0.5cm}for(g in 2:len)\{\\
\hspace{0.5cm}\hspace{0.5cm} f[g] $<-$ rbinom(1, size=N, prob=f[g-1])/N;\\
\hspace{0.5cm}\}\\
  lines(1:len, f, col=col[i], lwd=2);\\
\} }
\end{codeblock}
\end{frame}


%-----------------------------------------------------------------------------------------

\begin{frame}
\frametitle{Simulating the WF-model}

\textbf{Examples of simulated trajectories}\\\bigskip
\includegraphics[width=\columnwidth]{Figures/Example_simulated_trajectories.png}

\pause
\bigskip
\textbf{Note:} the magnitude of the change is a function of the population size.

\pause
\bigskip
\textbf{Note:} under drift alone, every allele will eventually become fixed or lost!
\end{frame}


%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Wright-fisher model for diploid organisms}

 Note that the two alleles sampled in a diploid individual have independent histories, as both were in a different individual one generation before.

 \bigskip

 \includegraphics[width=\textwidth]{Figures/diploid_WF.pdf}

 \bigskip

 We can thus model a diploid population as a haploid population of size $2N$ where a diploid individual corresponds to a random sample of two alleles.

\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Identity by state}
\begin{beamerboxesrounded}{Definition}
Let $G_t$ be the probability of identity-by-state for two randomly sampled alleles from generation $t$. Hence, $H_t=1-G_t$ is the expected heteFigures/rozygosity in a diploid population.
\end{beamerboxesrounded}

\begin{itemize}
\item If two alleles are descended from the same gene copy in the previous generation, they must be {\em identical-by-state}.
\item If two alleles are not descended from the same gene copy in the previous generation, they will be {\em identical-by-state} with probability $G_{t-1}$
\end{itemize}
{\small
\begin{align*}
G_t &=\frac{1}{N}+(1-\frac{1}{N})G_{t-1}\\
(1-G_t) &=(1-G_{t-1})(1-\frac{1}{N})\\
H_t& =H_{t-1}(1-\frac{1}{N})\\
H_t&=H_0(1-\frac{1}{N})^t\\
H_t&\approx H_0 e^{-\frac{t}{N}}
\end{align*}
}
\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Loss of variation due to genetic drift}
\begin{center}
$H_t\approx H_0 e^{-\frac{t}{N}}\rightarrow$ Heterozygosity decreases on a time-scale proportional to $N$.
\end{center}
\includegraphics[width=\textwidth]{Figures/GeneticDrift_10.png}
\end{frame}
%-----------------------------------------------------------------------------------------
\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize}
 \item Most loci in most diploid species are in HWE.
 \item However, there are processes that may lead to a deviation from HWE:
 \begin{itemize}
 \item Assortative mating
 \item Selection
 \item Population structure
 \item Inbreeding
 \end{itemize}
 \item Deviations from HWE are measured by the \emph{inbreeding coefficient} $F$

 \pause
 \item Since a sample is often a bit different from HWE by chance, we need to perform a statistical test to reject HWE.
 \item The $\chi^2$ test offers an easy way to compare categorical data to their expectations.
  \pause
  \item While we expect allele frequencies not to change under HWE, they do so by change in finite populations.
  \item The Wright-Fisher model is a simple model to explain this observation.
  \item Under this model, all alleles will eventually become fixed or lost.
 \end{itemize}
 \pause
 \smallskip
 \begin{block}{}
  For next week: please read \textbf{Chapter 2 (pages 27-33)} and solve Exercises 1!
 \end{block}


\end{frame}

%-----------------------------------------------------------------------------------------


\end{document}
