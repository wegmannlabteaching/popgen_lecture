#---------------------------------------------------------------------------------
#This script simulates allele trajectories under selection in a diploid population
#---------------------------------------------------------------------------------

#Note: viabilities are "rescaled" such that the population remains constant.
vAA <- 1.00;
vAa <- 0.94;
vaa <- 0.94;

#Parameters of the model
N <- 1000;
f0 <- 1/(2*N);
rep <- 100;
maxGen <- 1000;

#print s
fittest <- max(vAA, vAa, vaa);
print(paste("Selection corefficients:", vAA/fittest - 1, vAa/fittest - 1, vaa/fittest - 1))

#open plot
par(mfrow=c(1,1), mar=c(4,4,0.5,1));
plot(0, type='n', xlim=c(0, maxGen), ylim=c(0,1), xlab="Generation", ylab="Frequency of A", xaxs='i', yaxs='i');

#start loop over replicates
numLost <- rep;
for(r in 1:rep){
  f <- rep(0, maxGen);
  f[1] <- f0;
  #start loop over generations
  for(g in 2:maxGen){
    #selection
    fA <- f[g-1];
    fa <- 1-fA;    
    fPrime <- (vAA*fA*fA + vAa*fA*fa)/(vAA*fA*fA + vAa*2*fA*fa + vaa*fa*fa);

    #drift: assume constant population size
    f[g] <- rbinom(1, 2*N, fPrime) / (2*N);    

    #stop if at 0 or 1
    if(f[g]==0 || f[g]==1){ 
      if(g<maxGen){ f[(g+1):maxGen] <- f[g]; }
      break; 
    }
  }

  #plot trajectory
  if(f[maxGen]>0){
    numLost <- numLost-1;
    lines(1:maxGen, f, col='red');
  } else { lines(1:maxGen, f); }
}

#conclude
print(paste("In total, ", numLost, " (", round(100*numLost/rep), "%) were lost", sep=""));

