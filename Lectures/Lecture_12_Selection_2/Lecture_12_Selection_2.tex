\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Selection \& Drift}

%TOPIC Selection in finite populations
%TOPIC Substitution rates
%TOPIC Genetic Hitchhiking
%READING Chapter 8 (153 - 174)
%EXERCISES Exercises_8

\begin{document}
\maketitle

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Recap}

 \begin{itemize} 
       
    \item Selection may lead to more rapid changes in allele frequencies than expected under drift alone.
    
    \item While selection might act anywhere during the life cycle of an organism, we model \emph{viability selection} due to its simplicity.
    
    \item Allele frequency changes due to selection are driven by the relative fitness of the different genotypes.
    
    \item While dominant mutations increase the most quickly in frequency, they are the slowest to fix!
    
    \item Selection may also promote polymorphisms in the population, for instance through heterozygote advantage.
    
 \end{itemize}
 
 \pause
 \bigskip
 
 \textbf{Question:} how does genetic drift affect selected alleles?
  
\end{frame}

%------------------------------------------------------------------------
\section{Drift \& Selection}
\begin{frame}
 \frametitle{Simulating Drift \& Selection}
 
 In each generation:\\\bigskip
 
 \textbf{1) Simulate effect of viability selection}\\
 Each generation, viability selection is changing the allele frequency by
 
\begin{equation*}
 f_A'(t) = \displaystyle\frac{\upsilon_{AA}{f_A(t)}^2 + \upsilon_{Aa}f_A(t)f_a(t)}{\upsilon_{AA}{f_A(t)}^2 + 2\upsilon_{Aa}f_A(t)f_a(t) + \upsilon_{aa}{f_a(t)}^2} 
\end{equation*}\\\medskip
 
 \bigskip \pause
 \textbf{2) Simulate random mating}\\
 Each generation, the frequency after random mating is the result of binomial sampling:
 
 \begin{equation*}
  f_A(t+1) = \displaystyle \frac{N_A(t+1)}{2N} \quad \mbox{where} \quad 
P(N_A(t+1)=x|f_A'(t))={2N\choose x} f_A'(t)^x(1-f_A'(t))^{2N-x}
 \end{equation*}

  
\end{frame}

%------------------------------------------------------------------------
\begin{frame}<handout:0>
 \frametitle{Simulating Drift \& Selection}
 
 \textbf{Let's simulate!}

 
 \end{frame}
%------------------------------------------------------------------------
\section{Fixation probabilities}
\begin{frame}
 \frametitle{Fixation probabilities}

 \begin{columns}[T]
  \begin{column}{5cm}
   \textbf{Fixation probability}\\
   The probability of fixation of a new mutation is given by
   
   \begin{equation*}
    u(s,N) = \displaystyle \frac{1-e^{-2s}}{1-e^{-4Ns}}
   \end{equation*}
   
   \pause
   \textbf{Three domains}\\
   \begin{enumerate}
    \item If $2Ns>1$, the mutation is considered \emph{strongly advantageous} and $u(s,N)\approx 2s$.
    \item If $2Ns<-1$, the mutation is considered \emph{strongly deleterious} and $u(s,N)\approx 0$.
    \item If $-1<2Ns<1$, the mutation is considered \emph{nearly neutral} and $u(s,N)\approx \frac{1}{2N}$.
   \end{enumerate}

   
  \end{column}
\begin{column}{5.1cm}
 \includegraphics[width=\columnwidth]{Figures/fixProb.pdf}  
  \end{column}
 \end{columns}

 
 
 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Fixation probabilities}
 
 \textbf{Effect of genetic drift on selected mutations}\\
 
 \begin{itemize}
  \item Most advantageous mutations will be lost (unless $|s|>0.25$).
  \item Some deleterious mutations will fix by chance.
  \item The population size has a strong effect on \emph{nearly neutral} mutations ($-1<2Ns<1$), but not on \emph{strongly} selected mutations.
  \item \textbf{BUT:} the nearly neutral range depends on $N$!\\ $\Rightarrow$ Selection is much more efficient in large populations.
 \end{itemize}

\bigskip
\bigskip
\pause

\textbf{Note:} $u(s,N) = \displaystyle \frac{1-e^{-2s}}{1-e^{-4Ns}}$ is not defined for $s=0$, but $\displaystyle \lim_{s \rightarrow 0}\displaystyle \frac{1-e^{-2s}}{1-e^{-4Ns}} = \displaystyle \frac{1}{2N}$
 \end{frame}
%------------------------------------------------------------------------
\section{Substitution rates}
\begin{frame}
 \frametitle{Substitution rate}

   \textbf{A Substitution...}\\
  ... occurs if an ancestral allele is replaced by a novel, derived allele.\\\medskip
  
  \textbf{Substitution rate}\\
  The product of the population mutation rate and the probability of fixation.\\\bigskip
  
  For a neutral mutation, the substitution rate is $r = 2N \mu \displaystyle \frac{1}{2N} = \mu$.\\\bigskip\pause
  
  Analogously, for a selected allele, $r(s,N) = 2N\mu u(s,N) = 2N \mu \displaystyle \frac{1-e^{-2s}}{1-e^{-4Ns}}$
  
  \bigskip \pause
  
  \textbf{Implications}\\
  In contrast to neutral mutations, the substitution rate of selected mutations depends on the population size. Exceptions:
  \begin{itemize}
   \item For strongly deleterious mutations $r(s<\frac{1}{2N},N) \approx 0$.
   \item For strongly advantageous mutations $r(s>\frac{1}{2N},N) \approx 2s$.
  \end{itemize}

 
 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Substitution rates in coding regions}
 
 \begin{columns}[T]
  \begin{column}{5cm}
 \includegraphics[width=\columnwidth]{Figures/Genetic_Code.jpg}  
  \end{column}
  \begin{column}{5cm}
 \textbf{Degeneracy of the genetic code}  \\
 
 Many amino acids are encoded by multiple codons.\\\medskip
 
 For some amino acids, a mutation at the third position never changes the amino acid. Such positions are called \emph{fourfold degenerate}.\\\medskip
 
 \pause
 
 Fourfold degenerate sites are often assumed neutral and hence serve as an estimate of the local neutral substitution rate.\\\medskip
   \end{column}

 \end{columns}
 \pause
 
 \bigskip
 More sophisticated methods infer the rate of all mutations that do or do not alter the amino acids.\\\medskip
 These mutations are called \emph{nonsynonymous} and \emph{synonymous}, respectively.
  
 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Substitution rates in coding regions}
 
 \textbf{Substitution rates estimates}\\
  Li \textit{et al.} (1985) estimated substitution rates by comparing Human and Mice.\\\medskip
 
 
 \begin{tabular}{l c c c}
  \hline
  Gene & Synonymous rate & Nonsynonymous rate & \hiddencell{3}{$\alpha$}\\
  \hline
  Histone \textit{H3} & 6.38 & 0.00 & \hiddencell{3}{1.00}\\
  Histone \textit{H4} & 6.13 & 0.03 & \hiddencell{3}{1.00}\\
  $\alpha$-hemoglobin & 3.94 & 0.56 & \hiddencell{3}{0.86}\\
  Albumin & 6.72 & 0.92 & \hiddencell{3}{0.86}\\
  Prolactin & 5.59 & 1.29 & \hiddencell{3}{0.77}\\
  $\gamma$-interferon & 8.59 & 2.80 & \hiddencell{3}{0.67}\\\hline  
 \end{tabular}

 \bigskip
 \pause
 \textbf{Note:} differences in local mutation rates can not account for all the variation in nonsynonymous rates $\Rightarrow$ evidence for \emph{purifying} selection.

 \pause
 \bigskip
 
 \textbf{Estimating the fraction of strongly deleterious mutations $\alpha$}\\
 Assuming strongly deleterious mutations to never fix and all other mutations to behave neutrally, we get $r_{nonsyn} \approx (1-\alpha)\mu$ and $\mu = r_{syn} \Rightarrow \hat{\alpha} = 1-\displaystyle\frac{r_{nonsyn}}{r_{syn}}$
 
 
 \end{frame}
%------------------------------------------------------------------------
\section{Genetic Hitchhiking}
\begin{frame}
 \frametitle{Genetic hitchhiking}
 
 \begin{columns}[T]
  \begin{column}{6.8cm}
  
   \textbf{Selective sweep}\\
 If an advantageous mutation is driven to fixation.\\\medskip
 
 \textbf{Partial sweep}\\
 If an advantageous mutation has increased substantially in frequency but is not yet fixed.\\\bigskip
  
  \pause
 
 \textbf{Genetic Hitchhiking}\\
  During a selective sweep, linked neutral mutations get a free ride to higher frequency.\\\medskip
  
  As a result, most of the genetic diversity in the region is lost.\\\medskip
  
  Some neutral mutations may recombine onto a haplotype with the advantageous alleles and hence escape being lost.
 
    
  \end{column}
 \begin{column}{3cm}
 \includegraphics<1->[width=\columnwidth]{Figures/sweep.pdf}
   
  \end{column}

 \end{columns}

 
 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Genetic hitchhiking}
 
 \begin{overlayarea}{\textwidth}{\textheight}
 
 \begin{columns}[T]
\begin{column}{4.3cm}
  Consider two loci with alleles $A/a$ and $B/b$, of which, the $A$ allele is advantageous.
 
 
\end{column}

 \begin{column}{5.5cm}
   \begin{tabular}{l c c c c}
  \hline
  \textbf{Haplotype} & $AB$ & $Ab$ & $aB$ & $ab$\\
  \textbf{Frequency} & $f_{AB}$ & $f_{Ab}$ & $f_{aB}$ & $f_{ab}$ \\
  \textbf{Fitness} & 1 & 1 & $1-s$ & $1-s$\\
  \hline
 \end{tabular}
  \end{column}

 \end{columns}

\bigskip\pause

\textbf{Changes in haplotype frequencies per generation (genic selection)}\\\medskip
\begin{onlyenv}<2|handout:1>
\begin{tabular}{c c c c}
 \hline
 Genotype & $f$ after mating & Fitness &  Fraction $AB$ gametes\\\hline
 $AB/AB$ & ${f_{AB}}^2$ & 1 &  1\\
 $AB/Ab$ & $2f_{AB}f_{Ab}$ & $1$ &  $\frac{1}{2}$\\
 $AB/aB$ & $2f_{AB}f_{aB}$ & $1-s$  & $\frac{1}{2}$\\
 $AB/ab$ & $2f_{AB}f_{ab}$ & $1-s$  & $(1-c)\frac{1}{2}$\\
 $Ab/aB$ & $2f_{Ab}f_{aB}$ & $1-s$  & $c\frac{1}{2}$\\ 
 $Ab/Ab$ & ${f_{Ab}}^2$ & 1  & 0\\ 
 $Ab/ab$ & $2f_{Ab}f_{ab}$ & $1-s$  & 0\\
 $aB/aB$ & ${f_{aB}}^2$ & $(1-s)^2$ & 0\\
 $aB/ab$ & $2f_{aB}f_{ab}$ & $(1-s)^2$  & 0\\
 $ab/ab$ & ${f_{ab}}^2$ & $(1-s)^2$  & 0\\
 \hline
\end{tabular}
\end{onlyenv}
\begin{onlyenv}<3|handout:2>
\begin{tabular}{c c c c}
 \hline
 Genotype & $f$ after mating & Fitness &  Fraction $AB$ gametes\\\hline
 $AB/AB$ & ${f_{AB}}^2$ & 1 &  1\\
 $AB/Ab$ & $2f_{AB}f_{Ab}$ & $1$ &  $\frac{1}{2}$\\
 $AB/aB$ & $2f_{AB}f_{aB}$ & $1-s$  & $\frac{1}{2}$\\
 $AB/ab$ & $2f_{AB}f_{ab}$ & $1-s$  & $(1-c)\frac{1}{2}$\\
 $Ab/aB$ & $2f_{Ab}f_{aB}$ & $1-s$  & $c\frac{1}{2}$\\
 \hline
\end{tabular}


\begin{equation*}
 f_{AB}' = \displaystyle \frac{{f_{AB}}^2 + f_{AB}f_{Ab} + (1-s)f_{AB}f_{aB} + (1-s)f_{AB}f_{ab}(1-c) + (1-s)f_{Ab}f_{aB}c }{\bar{\omega}}
 \end{equation*}
 where $\bar{\omega} = (1-sf_a)^2$

\end{onlyenv}

\end{overlayarea}
 
 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Genetic hitchhiking}
 Numerical iteration of a new mutation for $N=1000$, $fB(0)=0.2$ and $s=0.1$.\\\medskip
 \includegraphics[width=\textwidth]{Figures/hitchkining_trajectories_expected.pdf}

 \pause\medskip
 \textbf{Note:} These are expected trajectories in the absence of drift!
 
 \end{frame}
%------------------------------------------------------------------------

\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize} 
       
    \item Genetic drift adds randomness to the trajectories of selected mutations.
    
    \item The fixation probability of selected alleles is affected by the population size.
    
    \item Mutations for which $-1 < 2Ns < 1$ are considered \emph{nearly neutral} and are strongly affected by drift.
    
    \item The fixation probability of strongly advantageous alleles is low ($2s$)
    
    \item And even some deleterious alleles have a chance to get fixed!
    
    \item This leads to different substitution rates for selected and neutral sites, which allows to estimate the strength of selection from species comparisons.
    
    \item Neutral alleles linked to selected alleles may hitchhike to considerable frequencies.
    
 \end{itemize}
 
 \pause
 \begin{block}{}  
  For next week, solve \textbf{Exercises 9} and read \textbf{Chapter 9}.\\\medskip
  We will also discuss the \textbf{example exam questions}!
 \end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
