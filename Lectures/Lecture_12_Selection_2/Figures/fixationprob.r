#function

fixProb <- function(s, N){
  return((1-exp(-2*s))/(1-exp(-4*N*s)));
}

s <- seq(-0.005, 0.005, length.out=100);

N <- c(1000, 200);
cc <- data.frame(r=c(164/255, 255/255), g=c(6/255, 125/255), g=c(255/255, 6/255));

col <- rgb(cc)
colt <- rgb(cc, alpha=0.3)

pdf("fixProb.pdf", width=4, height=6)
par(mfrow=c(2,1), mar=c(3,4,0,0), oma=c(1,1,0.3,0.5), las=1, xaxs='i', yaxs='i')
for(i in 1:length(N)){
  plot(s, fixProb(s, N=N[i]), type='l', col=col[i], lwd=2, ylab="", xlab="")   
  
  #add NS < 1 window
  sx <- 1 / (2*N[i]);
  polygon(c(-sx, sx, sx, -sx, -sx), par("usr")[c(3,3,4,4,3)], col=colt[i], border=NA)
  
  #add N
  text(par("usr")[1] + 0.01*(par("usr")[2]-par("usr")[1]), par("usr")[4] - 0.01*(par("usr")[4]-par("usr")[3]), labels=paste("N =", N[i]), adj=c(0,1));
  
  #add approximation
  lines(c(par("usr")[1], -sx, -sx, sx, sx, par("usr")[2]), c(0,0,1/2/N[i], 1/2/N[i], 2*sx, 2*par("usr")[2]), lty=2, lwd=2, col=col[i])
  
  mtext("Fixation probability", side=2, outer=F, line=4, las=3)
}

mtext("Selection coefficient", side=1, outer=F, line=3)

dev.off();