#params
fB0 <- 0.2;
s <- 0.1;
N <- 1000;

#function
hitchhike <- function (fB0 = 0.2, s=0.1, N=1000, c=0.005, G=100){
  #get initial haplotype frequencies
  fAB <- 1/(2*N);
  fAb <- 0;
  faB <- fB0 - fAB;
  fab <- 1-fB0;
  
  #iterate over generations
  fA <- numeric(G); fA[1] <- 1/(2*N);
  fB <- numeric(G); fB[1] <- fB0;
  
  for(g in 2:G){
    #precalculate wbar and D
    D <- fAB*fab - fAb*faB;
    fa <- 1 - fA[g-1];
    wbar <- (1-s*fa)^2;
    old <- g-1;    
    
    #get updated haplotype frequencies among gametes
    fAB <- (fAB - s*fAB*fa - c*(1-s)*D)/wbar;
    fAb <- (fAb - s*fAb*fa + c*(1-s)*D)/wbar;
    faB <- ((1-s)*(faB -s*faB*fa+c*D))/wbar;
    fab <- 1 - fAB - fAb - fab;
    
    #update allele frequencies
    fA[g] <- fAB + fAb;
    fB[g] <- fAB + faB;
  }
  
 return(data.frame(g=1:G, fA=fA, fB=fB));  
}

#plot
pdf("hitchkining_trajectories_expected.pdf", width=6, height=3.5)
par(mar=c(4,4,0.5,4.5), las=1)
res <- hitchhike(fB0=fB0, s=s, N=N, c=0.0, G=120);
plot(res$g, res$fA, type='l', lwd=2, col='red', ylim=c(0,1), xaxs='i', yaxs='i', ylab="Allele frequency", xlab="Generation")
legend('topleft', legend=c("fA", "fB"), col=c('red', 'black'), lwd=2, bty='n');

rec <- c(0.0, 0.01, 0.1);
lty <- 1:3;
at <- c();

for(i in 1:length(rec)){
  res <- hitchhike(fB0=fB0, s=s, N=N, c=rec[i], G=120);
  lines(res$g, res$fB, col='black', lty=lty[i], lwd=2)
  at <- c(at, max(res$fB));
}

axis(side=4, at=at, labels=paste("c =", rec));
dev.off();

