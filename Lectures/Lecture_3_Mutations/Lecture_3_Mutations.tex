\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Mutation}

%TOPIC Mutations
%TOPIC Fixation probabilities
%TOPIC The molecular clock and divergence between species
%READING Chapter 2 (pages 27 - 33)
%EXERCISES Exercises_1

%TODO: Add simple exercise to discuss if there is too much time!

\begin{document}
\maketitle

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Genetic drift}


\begin{enumerate}
  \item While we expect allele frequencies not to change under HWE, they do so due to finite population sizes.
  \item The Wright-Fisher model is a simple model to explain this observation.
  \item Under this model, all alleles will eventually become fixed or lost.
  \item The rate of change per generation is a direct function of the population size.
 \end{enumerate}

 \pause
 \bigskip

 \textbf{Questions:} Why is there still genetic diversity? And how does it arise?

\end{frame}
%-----------------------------------------------------------------------------------------
\section{Mutations}
\begin{frame}
  \frametitle{Luria \& Delbrück - or how novel traits arise}

 \begin{itemize}
  \item Most bacterial cells of  a phage-sensitive culture onto a plate containing phages will die.  \item But some actually grow and transmit the acquired resistance to their offspring.\bigskip
  \item Two hypothesis:
  \begin{enumerate}
   \item With some probability, a bacterium survives a phage attack. This survival confers immunity not only to the individual but also to its offspring.
   \item With some probability, a bacterium mutates from ``sensitive'' to ``resistant'' at a given time point, regardless of the presence of phages.
  \end{enumerate}\bigskip
  \pause
  \item How to tell these apart?\\$\Rightarrow$ Luria \& Delbrück (1941) found an elegant way...

 \end{itemize}

\end{frame}
%-----------------------------------------------------------------------------------------

\begin{frame}
\frametitle{Fluctuation test after Luria \& Delbrück (1941)}

 \begin{columns}[t] % contents are top vertically aligned
     \begin{column}{5cm}
     \begin{beamercolorbox}[wd=\textwidth,rounded=true,shadow=true]{block body}
	 \setstretch{1}The fraction of resistant bacteria is very similar when the plating is replicated.
     \end{beamercolorbox}
     \end{column}
     \begin{column}{5cm}
        \onslide<2-|handout:1>{\begin{beamercolorbox}[wd=\textwidth,rounded=true,shadow=true]{block body}
	 \setstretch{1}The fraction of resistant bacteria is highly variable among replicated cultures.
       \end{beamercolorbox}}
     \end{column}
 \end{columns}

 \medskip
 \includegraphics<1|handout:0>[width=\textwidth]{Figures/luria_delbruck_1.pdf}
 \includegraphics<2|handout:0>[width=\textwidth]{Figures/luria_delbruck_2.pdf}
 \includegraphics<3|handout:1>[width=\textwidth]{Figures/luria_delbruck_3.pdf}

\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Mutations}

 \textbf{Mutations come in many forms}\\
 \medskip
 \includegraphics[width=\textwidth]{Figures/mutTypes.pdf}

 \pause
 \bigskip
 \begin{alertblock}{}
  All these (and most other) mutations can be modeled with two alleles: \\
  one with the mutation and one without.
 \end{alertblock}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Mutation rates}

 \textbf{How frequent do mutations occur?}\\\medskip

  Mutation rates for Single Nucleotide Variants (SNV) estimated form Icelandic pedigrees.\\\medskip

 \includegraphics[width=\columnwidth]{Figures/KongMutRates.png}

  \pause
  \bigskip
  \begin{alertblock}{}
  Mutation rates are on the order of $\mu=1.2\cdot10^{-8}$ per generation per base pair.
 \end{alertblock}

  %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Kong \textit{et al}. 2012} \hspace{.5em}
 \end{textblock*}}
 
\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Mutation rates}

 \textbf{What does $\mu=1.2\cdot10^{-8}$ imply?}\\
 \begin{itemize}
  \item Mutations are very rare. Imagine doing a single spelling mistake when copying LOTR 26 times by hand!
  \item But since our genome is $3.2\cdot 10^9$ base pairs long, we still expect $2\cdot3.2\cdot 1.2\cdot10^9\cdot10^{-8}=76.7$ new mutations per individual per generation!
  \pause\bigskip
  \item Since we are $8\cdot10^9$ individuals on this planet, the probability $m$ that no single person has a mutation at a specific base pair is
  \begin{equation*}
   m=(1-1.2\cdot10^{-8})^{2\cdot8\cdot10^9}=4.1\cdot 10^{-84},
  \end{equation*}
  which is less likely that two people pick the the same atom in the universe at random!
 \end{itemize}



\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Effect of mutations on allele frequencies}

 \textbf{Will new mutations affect allele frequencies?}\\
 Assume there are two alleles $A$ and $a$ at frequencies $f_A(t)$ and $f_a(t) = 1-f_A(t)$.\\\medskip
 \textbf{Question:} Given the per generation mutation rates $\mu_{a\rightarrow A}$ and $\mu_{A\rightarrow a}$, what are the expected allele frequencies in the next generation $t+1$?

 \pause
  \begin{equation*}
  E[f_A(t+1)] = (1-\mu_{A\rightarrow a})f_A(t) + \mu_{a \rightarrow A}(1-f_A(t))
 \end{equation*}
\pause
%\smallskip

\textbf{Question:} what will happen in the long term?\\
\pause
In the absence of other forces (e.g. genetic drift or selection), an equilibrium frequency $\widetilde{f_A}$ will be established such that
\begin{align*}
  \widetilde{f_A} &=  (1-\mu_{A\rightarrow a})\widetilde{f_A} + \mu_{a \rightarrow A}(1-\widetilde{f_A})\\
  &= \widetilde{f_A} -\mu_{A\rightarrow a}\widetilde{f_A} + \mu_{a \rightarrow A} - \mu_{a \rightarrow A}\widetilde{f_A}\\
  \widetilde{f_A}(\mu_{A\rightarrow a} + \mu_{a \rightarrow A}) &= \mu_{a \rightarrow A}\\
  \widetilde{f_A}&=\frac{\mu_{a \rightarrow A}}{\mu_{A\rightarrow a} + \mu_{a \rightarrow A}}
\end{align*}


\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Effect of mutations on allele frequencies}

 \textbf{How long will it take to establish this equilibrium?}\\\bigskip

 \includegraphics[width=\textwidth]{Figures/timeToEquilibriumMutationrate.pdf}

 \bigskip

 Assuming $\mu=1.2\cdot10^{-8}$, it will take $> 2\cdot 10^8$ generations.  Assuming 25 years per generation, this mounts to $5\cdot10^9$ years, which is more than Earth exists!\\

 \pause
 \bigskip
 \begin{alertblock}{}
  The effect of recurrent mutations on allele frequencies can safely be ignored.
 \end{alertblock}
\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Fate of a new mutation}

 \textbf{Most new mutations will disappear again}\\
 Eventually, all mutations will either get fixed or be lost.\\
 Most new mutations will be lost.

 \bigskip
 \includegraphics[width=\textwidth]{Figures/fixprob_Newmut_200.pdf}

 \pause
 \bigskip
 \textbf{Question:} what is the probability that a new mutation will fix?

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Fate of a new mutation}

 \textbf{Probability of fixation}\\
 \begin{itemize}
  \item Each allele will eventually be fixed or get lost.
  \item This is also true if we define an allele as the copy of one individual.
  \item In the absence of selection, each such allele should have the same probability to become fixed. And if one fixes, all others will be lost.
  \item Hence, the probability that the allele of one individual will fix is $\displaystyle \frac{1}{2N}$ in a diploid population.
  \item If an allele is present in $N_A$ copies, it's probability of fixation is $\displaystyle \frac{N_A}{2N}=f_A$
 \end{itemize}
\pause
 \begin{alertblock}{}
  Under genetic drift, the probability of fixation is simply the allele frequency!
 \end{alertblock}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Rate of Substitution}

  \textbf{A Substitution...}\\
  ... occurs if an ancestral allele is replaced by a novel, derived allele.\\\medskip

  \textbf{Question:} what is the rate of substitution in a diploid population of size $N$ and mutation rate $\mu$?\\
  \pause
  \begin{itemize}
   \item Each gene copy has a probability $\mu$ to be affected by a mutation. Hence, the expected number of mutations entering the population per generation is $2N\mu$.
   \item Each of those new mutations has a probability of $\frac{1}{2N}$ to become fixed.
   \item The expected number of mutations that enter the population each generation and will eventually fixed (the number of substitutions $S$) is thus
   \begin{equation*}
    E(S) = 2N\mu \cdot \frac{1}{2N} = \mu
   \end{equation*}
  \end{itemize}

\pause
 \begin{alertblock}{}
  Under genetic drift, the rate of substitution is = mutation rate and does not depend on the population size!
 \end{alertblock}
\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Molecular clock}


  \begin{columns}[T]
  \begin{column}{4cm}
   \includegraphics[width=\columnwidth]{Figures/human_chimp_div.pdf}
  \end{column}
  \begin{column}{6.5cm}
   \begin{itemize}
  \item Per generations, we expected $\mu$ substitutions at each nucleotide.
  \item Over $t$ generations, we thus expect $t\mu$ substitutions / nucleotide.
  \item Between two species that diverged $t$ generations ago, we thus expect $2t\mu$ substitutions / nucleotide.
 \end{itemize}
  \end{column}
  \end{columns}

  \pause
  \bigskip
  \begin{block}{Definition: Molecular clock}
   The molecular divergence between pairs of species is directly proportional to the divergence time.
  \end{block}

 \end{frame}

%-----------------------------------------------------------------------------------------

 \begin{frame}
 \frametitle{Molecular clock}

 \textbf{Molecular clock in birds}\\\bigskip

  \begin{columns}[T]
  \begin{column}{6cm}
 \includegraphics[width=\columnwidth]{Figures/molecularClockBirds.pdf}
 \end{column}
  \begin{column}{4cm}
  The molecular divergence between many pairs of birds matches well with both biogeographical (black diamonds) and fossil (open circles) calibrations.

  \end{column}
  \end{columns}
  
  %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Weir and Schluter 2008} \hspace{.5em}
 \end{textblock*}}
\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Estimating divergence times (1)}

 \textbf{How long ago did humans and chimpanzees diverge?}\\
 Humans and chimpanzees differ at $\sim$ 1.2\% of the nucleotide positions. Knowing that $\mu \approx 1.2\cdot10^{-8}$, how long ago did they diverge?\\\bigskip

 \pause
 \bigskip
 \begin{itemize}
  \item We have $0.012 = 2t\mu = 2t\cdot 1.2\cdot10^{-8}$, which we can solve to
 \begin{equation*}
  t = \displaystyle \frac{0.012}{2\cdot1.2\cdot10^{-8}}=5\cdot 10^5 \mbox{ generations}
 \end{equation*}

 \item Assuming 25 years per generation, this suggest that divergence occurred around 12.5 million years ago.

 \end{itemize}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Estimating divergence times (2)}

 \textbf{How long ago did humans and chimpanzees diverge?}\\

 Fossil evidence suggests that Cercopithecoid primates (such as the Rhesus Macaque) and hominoids (such as humans and chimpanzees) diverged 25 million years ago.\\\medskip
 \textbf{Question: }Knowing that humans and macaques differ at 7\% of the nucleotide positions and human and chimpanzees at 1.2\%, how long ago did humans and chimpanzees diverge?


 \pause
 \bigskip
 \begin{itemize}
 \item Since $0.07 = 2\cdot 25\cdot10^6\cdot\mu$, we can estimate $\mu = 1.4\cdot 10^{-9}$ per year.
 \bigskip

\item We thus have $0.012 = 2t\mu = 2t\cdot 1.4\cdot10^{-9}$, which we can solve to
 \begin{equation*}
  t = \displaystyle \frac{0.012}{2\cdot1.4\cdot10^{-9}}=4.3\cdot 10^6 \mbox{ years}
 \end{equation*}
\end{itemize}
\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Estimating divergence times (2)}

 \textbf{Why do these numbers not match?}\\
 We have now two different estimates for the human-chimpanzee divergence: 4.3 and 12.5 million years. Why do they not match?\\

 \bigskip
 \pause
 \textbf{Molecular clock assumes constant generation time}\\
 If generation times differ, the molecular clock of a constant rate of substitutions per year is violated. The generation time of macaques is about 12 years, not 25 like in humans.\\\bigskip

 \textbf{Molecular clock assumes constant mutation rate}\\
Recent evidence suggests that the mutation in humans has been reduced over the last 10,000 years, so $\mu=1.2\cdot10^{-8}$ may be a good estimate for current rates, but not ancestral rates.\\\bigskip

 \pause
 \begin{alertblock}{}
    Taken all genetic and fossil evidence together, humans and chimpanzees most likely diverged 5-6 million years ago.
 \end{alertblock}


\end{frame}


%-----------------------------------------------------------------------------------------
\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize}
 \item Over time, genetic drift is reducing genetic diversity.
 \item the reason we still observe genetic diversity are new mutations.
 \item Mutations are very rare. Single nucleotide variants (SNVs) occur with a rate of about $\mu=1.2\cdot10^{-8}$ per generation per nucleotide position.
 \item But this mounts to still about 75 mutations per individual per generation. And given the large number of people, most mutations occur in every generation.

 \pause
 \item Recurrent mutations do not affect allele frequencies much: we will thus ignore their effect in the future.
 \item The probability of fixation is equal to the allele frequency. New mutations thus fix with a probability of $\frac{1}{2N}$
 \item Since the number of new mutations is $2N<mu$, the number of fixing mutations is independent of the population size $\Rightarrow$ molecular clock!
 \end{itemize}

 \pause
 \begin{block}{}
  For next week: please read \textbf{Chapter 3 (pages 35 - 48)} and solve \textbf{Exercises 2}!
 \end{block}


\end{frame}


\end{document}
