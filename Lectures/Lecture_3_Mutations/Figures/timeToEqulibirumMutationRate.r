
mu <- 1.2*10^-8
oneMinusMu <- 1-mu;

len <- 10^9
step <- 10000
iter <- len / step

f<-0;
g<-rep(0, iter)
for(i in 1:iter){  
  for(j in 1:step){
     f <- oneMinusMu*f + mu*(1-f)
  }     
  g[i] <- f
  print(paste("Iteration", i , "of", iter))
}

n <- 0:(length(g)-1) * step / 10^8;

sub <- seq(1,length(g)*0.2501, length.out=1000)

pdf("timeToEquilibriumMutationrate.pdf", width=6, height=2.5)
par(mfrow=c(1,1), mar=c(3,3,0.2,0.6), oma=c(0,0,0,0), mgp=c(2, 0.5,0))
plot(0, type='n', xlim=c(0,n[sub[length(sub)]]), ylim=c(0, 0.5), xlab="Generations x 10^8", ylab="Allele frequency fA", xaxs='i')
lines(par("usr")[1:2], rep(0.5,2), type='l', lty=2, lwd=2, xlim=c(0,n[sub[length(sub)]]), ylim=c(0, 0.5), xlab="Generations x 10^8", ylab="Allele frequency fA", xaxs='i')
lines(n[sub], g[sub], type='l', col="red", lwd=2)
dev.off()
