N <- 200; f_init <- 1/N;
len <- 1000;
rep <- 1000;

#matrix solution
f <- matrix(data=0, nrow=len, ncol=rep);
f[1,] <- f_init;
for(g in 2:len){
  f[g,] <- rbinom(rep, size=N, prob=f[g-1,])/N;
}  
#plot
#pdf(paste("fixprob_Newmut_", N, ".pdf", sep=""), width=7, height=3)
par(mar=c(4,4,0.5,3))
plot(0, type='n', xlim=c(1,len), ylim=c(0,1), xlab="Generations", ylab="Allele frequency")
col <- rep("gray", rep);
col[f[len,]==1] <- "red";

for(i in 1:rep){
  lines(1:len, f[,i], col=col[i], lwd=1);
}
axis(side=4, at=c(0,1), labels=c(sum(f[len,]==0), sum(f[len,]==1)), las=1)

#text(par("usr")[2] - (par("usr")[2]-par("usr")[1]) * 0.02, par("usr")[4] - (par("usr")[4]-par("usr")[3]) * 0.02, labels=paste(rep, "replicates, N =", N), adj=c(1,1));

#dev.off()


