\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Coalescent Theory 2}

%TOPIC Coalescent with multiple individuals (second part)
%TOPIC Expected tree height and tree length
%TOPIC Site frequency spectrum
%TOPIC Effect of demography on tree shape
%READING Chapter 3 (pages 41-57)
%EXERCISES Exercises_3

%-----------------------------------------------------------
%IMPORTANT: R script for figures is in folder of lecture 4!
%-----------------------------------------------------------

\begin{document}
\maketitle

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Coalescent Theory of two samples}

 \begin{itemize}
 \item Coalescent theory is a population genetic theory that looks at the world \emph{backward in time}.
 \item This makes many calculations much simpler.

 \pause

 \item The probability that two individuals coalesce is $\frac{1}{2N}$ per generation.
 \item Two individuals differ on average by $\theta = 4N\mu$ mutations $\Rightarrow$ the average pairwise differences are an estimate of $\theta$.
 \item Coalescent theory applies to many non WF models when using an \emph{effective population size}.
 \end{itemize}

 \pause
 \bigskip
 \textbf{Question:} how do genealogies of multiple samples look like?\\\bigskip

 \textbf{Note:} we will use the term $n$ \emph{samples} to mean $n$ chromosomes or lineages. If you sample 10 diplodid individuals, $n=20$!


\end{frame}
%-----------------------------------------------------------------------------------------
 \section{Coalescent with multiple samples}

  \begin{frame}
  \frametitle{Coalescence with multiple samples}

  \textbf{Probability of coalescent}\\

   $$P(\mbox{at least one coalescent event}) = {k \choose 2}\frac{1}{2N} = \frac{k(k-1)}{4N}$$

  \textbf{Intuitive explanation}\\
  Probability of coalescence among $k$ lineages = probability of coalescence among two lineages $\frac{1}{2N}$ times the number of possible pairs ${k \choose 2}$.
  \\
  \bigskip\bigskip

  \pause

  \textbf{Expected time $t_k$ until first coalescent event among $k$ lineages}\\  
  The expected waiting time until an event occurs the first time is given by the inverse of the probability of the event!\\

  $$\E[t_k]=\frac{1}{{k \choose 2}\frac{1}{2N}}=\frac{2N}{{k \choose 2}} = \frac{4N}{k(k-1)}$$

 \end{frame}

 %-----------------------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Expected genealogy of $n$ samples (lineages)}

    \begin{columns}[T]
   \begin{column}{5.3cm}
  \includegraphics<1| handout:0>[width=\columnwidth]{Figures/time_epochs_begin.pdf}
  \includegraphics<2| handout:0>[width=\columnwidth]{Figures/time_epochs_t5.pdf}
  \includegraphics<3| handout:0>[width=\columnwidth]{Figures/time_epochs_t4.pdf}
  \includegraphics<4| handout:0>[width=\columnwidth]{Figures/time_epochs_t3.pdf}
  \includegraphics<5-6>[width=\columnwidth]{Figures/time_epochs_t2.pdf}
   \end{column}
  \begin{column}{5.1cm}
\begin{picture}(320,250)
\onslide<1-6>\put(10,235){\begin{minipage}[t]{\columnwidth}$\E[t_k] = \displaystyle \frac{1}{{k \choose 2}\frac{1}{2N}}=\frac{2N}{{k \choose 2}}=\frac{4N}{k(k-1)}$\end{minipage}}
\onslide<2-6>\put(10,63){\begin{minipage}[t]{\columnwidth}$\E[t_5] = \displaystyle \frac{2N}{{5 \choose 2}}=\frac{2N}{10}$\end{minipage}}
\onslide<3-6>\put(10,97){\begin{minipage}[t]{\columnwidth}$\E[t_4] = \displaystyle \frac{2N}{{4 \choose 2}}=\frac{2N}{6}$\end{minipage}}
\onslide<4-6>\put(10,130){\begin{minipage}[t]{\columnwidth}$\E[t_3] = \displaystyle \frac{2N}{{3 \choose 2}}=\frac{2N}{3}$\end{minipage}}
\onslide<5-6>\put(10,183){\begin{minipage}[t]{\columnwidth}$\E[t_2] = \displaystyle \frac{2N}{{2 \choose 2}}=2N$\end{minipage}}
\onslide<6-6>\put(-15,55){\begin{minipage}[t]{1.15\columnwidth}\begin{alertblock}{}Relative proportions are independent of $N$!\end{alertblock}\end{minipage}}
\end{picture}
 \end{column}
  \end{columns}
 \end{frame}
  %-----------------------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Expected genealogy of $n$ samples (lineages)}

    \begin{columns}[T]
   \begin{column}{5.3cm}
  \includegraphics[width=\columnwidth]{Figures/time_epochs_tmrca.pdf}
   \end{column}
  \begin{column}{5.1cm}
    \textbf{Expected total tree height $E[t_{MRCA}]$}\\\bigskip
  \begin{picture}(320,250)
  \onslide<2->\put(0,235){\begin{minipage}[t]{\columnwidth}$\E[t_{MRCA}] = \displaystyle \sum_{k=2}^{n}\E[t_k]=\displaystyle \sum_{k=2}^{n}\frac{4N}{k(k-1)}$\end{minipage}}
  \onslide<3->\put(0,200){\begin{minipage}[t]{\columnwidth}$= \displaystyle 4N\sum_{k=2}^{n}\frac{1}{k(k-1)}$\end{minipage}}
  \onslide<4->\put(0,165){\begin{minipage}[t]{\columnwidth}$= \displaystyle 4N\sum_{k=2}^{n}\frac{k-k+1}{k(k-1)}$\end{minipage}}
  \onslide<5->\put(0,130){\begin{minipage}[t]{\columnwidth}$= \displaystyle 4N\left(\sum_{k=2}^{n}\frac{k}{k(k-1)} - \sum_{k=2}^{n}\frac{k-1}{k(k-1)}\right)$\end{minipage}}
  \onslide<6->\put(0,95){\begin{minipage}[t]{\columnwidth}$= \displaystyle 4N\left(\sum_{k=2}^{n}\frac{1}{k-1} - \sum_{k=2}^{n}\frac{1}{k}\right)$\end{minipage}}
  \onslide<7->\put(0,60){\begin{minipage}[t]{\columnwidth}$= \displaystyle 4N \left(1-\frac{1}{n}\right)$\end{minipage}}


  \end{picture}

 \end{column}
  \end{columns}
 \end{frame}
 %-----------------------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Expected genealogy of $n$ samples (lineages)}
  \begin{columns}[T]
   \begin{column}{5.8cm}
  \includegraphics[width=\columnwidth]{Figures/TreeHeight.pdf}
   \end{column}
  \begin{column}{4.0cm}


  $\E[t_k]=\displaystyle \frac{2N}{{k \choose 2}} = \frac{4N}{k(k-1)}$\\\bigskip


  $\E[t_{MRCA}] = \displaystyle 4N \left(1-\frac{1}{n}\right)$\\\bigskip\bigskip

  \pause
  $\displaystyle \frac{\E[t_k]}{\E[t_{MRCA}]} = \frac{1}{k(k-1)}\frac{1}{1-\frac{1}{n}}$\\\bigskip\bigskip

  \pause
  \textbf{When $n$ is large:}\\\medskip $\displaystyle \frac{\E[t_k]}{\E[t_{MRCA}]} \approx \frac{1}{k(k-1)}$ \\\bigskip

 \end{column}
  \end{columns}
  \bigskip
  \pause
\textbf{Note:} the relative proportions are independent of $N$!\\
\textbf{Note:} there are only few lineages for most of the genealogy!

 \end{frame}

 %-----------------------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Expected genealogy of $n$ samples (lineages)}

  \textbf{Expected total length $L_n$ of the genealogy}\\
  The expected total length of a genealogy of $n$ samples, $L_n$ is the sum of the expected epoch length times the number of lineages during that epoch.


   \begin{columns}[T]
   \begin{column}{5.1cm}
  \includegraphics[width=\columnwidth]{Figures/time_epochs.pdf}
   \end{column}
  \begin{column}{4.7cm}

    \begin{eqnarray*}
      \E[L_n] &=&\sum_{k=2}^{n}\E[t_k]k\\\pause
      &=&\sum_{k=2}^{n}\frac{4N}{k(k-1)}k\\\pause
      &=& 4N \sum_{k=2}^{n}\frac{1}{k-1}\\
      &=& 4N \sum_{k=1}^{n-1}\frac{1}{k}
  \end{eqnarray*}


 \end{column}
  \end{columns}

 \end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Expected genealogy of $n$ samples (lineages)}

  \textbf{Height versus length of a genealogy of $n$ samples}\\\bigskip

 \begin{columns}
  \begin{column}{3.6cm}
  $$\E[T_n] = 4N \left(1-\frac{1}{n}\right)$$
  $$\E[L_n] = 4N \sum_{k=1}^{n-1}\frac{1}{k}$$
  \end{column}
\begin{column}{6.2cm}
\includegraphics[width=\columnwidth]{Figures/TreeHeightVsLength.pdf}
\end{column}
 \end{columns}
 \bigskip
 \pause
 \textbf{Note:} Adding additional samples does increase the expected tree height only marginally, but increases the tree length a lot.\\
 Actually, doubling the sample size increases the tree length by about 1.5 $N$.
\end{frame}
   %-----------------------------------------------------------------------------------------
   \section{Mutations on the Coalescent}
  \begin{frame}
  \frametitle{Mutations on the Coalescent}

  \textbf{Expected number of mutations}\\
  The expected number of mutations $\E[S]$ on a genealogy is given by the product of the expected length of the genealogy $\E[L_n]$ and the mutation rate $\mu$:\\\medskip
  $\E[S] = \E[L_n] \mu = 4N\mu \displaystyle \sum_{k=1}^{n-1}\frac{1}{k} = \theta  \sum_{k=1}^{n-1}\frac{1}{k}$ \\\bigskip

  \pause
  \textbf{Infinite Sites Model (ISM)}\\
  Assumes that each mutation hits a different base pair. This is realistic if $\theta \ll 1$.\\\medskip
    \pause
  \textbf{The Watterson estimator of $\theta$ under ISM}\\
  Under ISM, the number of mutations = to the number of polymorphic (called segregating) sites $S$ among samples. $S$ can thus be used to estimate $\theta$:\\\medskip

  $$\E[S] = \displaystyle \theta  \sum_{k=1}^{n-1}\frac{1}{k} \mbox{\hspace{1cm}} \Rightarrow  \mbox{\hspace{1cm}} \hat{\theta}_W = \frac{S}{ \displaystyle \sum_{k=1}^{n-1}\frac{1}{k}}$$


  \end{frame}
      %-----------------------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Example: the Watterson estimator of $\theta$}

   \begin{block}{Example: Sequence data from four samples (lineages)}
   \texttt{Sample 1: aggtatgcta gaaccctaga aagacacaga gatagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 2: agg\textcolor{red}{c}atgcta gaaccctaga aagac\textcolor{red}{g}caga gatagacaag tgttcattca}\\
   \texttt{Sample 3: agg\textcolor{red}{c}atgcta gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 4: aggtatgct\textcolor{red}{g} gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}
  \end{block}


  \begin{eqnarray*}
  \hat{\theta}_W = \displaystyle \frac{S}{\displaystyle \sum_{k=1}^{n-1}\frac{1}{k}} \pause = \frac{5}{1+\frac{1}{2}+\frac{1}{3}} = \frac{5}{\frac{11}{6}} = \frac{30}{11} = 2.72
  \end{eqnarray*}

  \pause

  \textbf{Note:} to compare numbers between studies, it is best to report $\hat{\theta}_W$ / site:

  \begin{equation*}
   \hat{\theta}_W =  \frac{30}{11 * 50} = \frac{30}{550} = 0.055
  \end{equation*}
  \pause
  \textbf{Comparison with $\hat{\theta}_T$}\\\pause
  $\hat{\theta}_W=0.055$ is close yet not identical to $\hat{\theta}_T = 0.057$ as both are \emph{estimates} of  $\theta$!


  \end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Site Frequency Spectrum (SFS)}
 %\textbf{The Site Frequency Spectrum (SFS)}\\
 The SFS $f$ tabulates the sample allele frequencies of all mutations.\\\smallskip

 Each entry $f_1$, $f_2$, ..., $f_{n-1}$ is the number (or proportion) of mutations at which 1, 2, ..., $n-1$ \textbf{derived} alleles have been observed in a sample of size $n$.

 \pause
   \begin{block}{Example: Sequence data from four samples (lineages)}
   \texttt{Sample 1: aggtatgcta gaaccctaga aagacacaga gatagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 2: agg\textcolor{red}{c}atgcta gaaccctaga aagac\textcolor{red}{g}caga gatagacaag tgttcattca}\\
   \texttt{Sample 3: agg\textcolor{red}{c}atgcta gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 4: aggtatgct\textcolor{red}{g} gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}
  \end{block}
  \pause
  \begin{columns}[T]
   \begin{column}{5.8cm}
  There are 5 mutations in total. Of those,\\
  $f_1=2$ have 1 derived allele copy,\\
  $f_2=2$ have 2 derived allele copies,\\
  $f_3=1$ have 3 derived allele copies.
   \end{column}
\begin{column}{4cm}
\pause
 \includegraphics[width=\columnwidth]{Figures/example_SFS.pdf}
\end{column}

  \end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Site Frequency Spectrum (SFS)}

 \begin{columns}[T]
 \begin{column}{4.5cm}
   %\includegraphics<1|handout:0>[width=\columnwidth]{geno_for_expected_sfs_2.pdf}
   %

   \includegraphics<1-2|handout:0>[width=\columnwidth]{Figures/geno_for_expected_sfs_2.pdf}
   \includegraphics<3->[width=\columnwidth]{Figures/geno_for_expected_sfs_3.pdf}
   \onslide<7>{
    \begin{alertblock}{}
     \textbf{Note:} We expect the same number of singletons among 2 and 3 samples!
    \end{alertblock}
   }
  \end{column}

   \begin{column}{5.3cm}
    \textbf{Expected SFS when $n=2$}\\
    All mutations are singletons:
    \begin{eqnarray*}
    \E[f_1] = \pause 2\E[t_2]\mu = 4N\mu=\theta
    \end{eqnarray*}

    \pause

    \textbf{Sample of $n=3$}\\
    Mutations falling on a branch with only one leave result in singletons.
    \begin{eqnarray*}
     \E[f_1] &=& \pause (3\E[t_3]+\E[t_2])\mu\\
     &=& \left(3\frac{2N}{3} + 2N\right)\mu =  4N\mu=\theta
    \end{eqnarray*}
    \pause
    Mutations falling on branches with two leaves result in doubletons.
    \pause
    \begin{align*}
     \E[f_2] = \E[t_2]\mu = 2N\mu = \frac{\theta}{2}
    \end{align*}
  \end{column}


 \end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Site Frequency Spectrum (SFS)}
    \textbf{Expected SFS when $n=4$}\\
    There are two different topologies with probabilities $\frac{1}{3}$ and $\frac{2}{3}$, respectively!\\\medskip


 \begin{columns}[T]
   \begin{column}{4.5cm}
   \includegraphics<1->[width=\columnwidth]{Figures/geno_for_expected_sfs_4.pdf}
  \end{column}


   \begin{column}{5.3cm}
    \pause
    \begin{eqnarray*}
     \E[f_1] &=& \frac{1}{3}(4\E[t_4] + 2\E[t_3])\mu \\
     &+& \frac{2}{3}(4\E[t_4]+2\E[t_3]+\E[t_2])\mu \\\pause
     &=&\left(4\E[t_4] + 2\E[t_3] + \frac{2}{3}\E[t_2]\right)\mu\\
     &=&\left(4\frac{2N}{6} + 2\frac{2N}{3} + \frac{2}{3}2N\right)\mu\\
     &=&\left(\frac{8}{6} + \frac{8}{6} + \frac{8}{6}\right)N\mu = 4N\mu = \theta
    \end{eqnarray*}
    \pause
    \begin{alertblock}{}
     \textbf{Note:} We expect the same number of singletons among 2, 3 and 4 samples!
    \end{alertblock}

  \end{column}

 \end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Site Frequency Spectrum (SFS)}
    \textbf{Expected SFS when $n=4$}\\
    There are two different topologies with probabilities $\frac{1}{3}$ and $\frac{2}{3}$, respectively!\\\medskip


 \begin{columns}[T]
 \begin{column}{4.5cm}
   \includegraphics<1->[width=\columnwidth]{Figures/geno_for_expected_sfs_4.pdf}


  \end{column}

   \begin{column}{5.3cm}

    \begin{eqnarray*}
     \E[f_2] &=& \left(\frac{1}{3}(\E[t_3] + 2\E[t_2])+ \frac{2}{3}\E[t_3]\right)\mu \\\pause
     &=&\left(\E[t_3] + \frac{2}{3}\E[t_2]\right)\mu\\
     &=&\left(\frac{2N}{3} + \frac{2}{3}2N\right)\mu = 2N\mu = \frac{\theta}{2}
    \end{eqnarray*}

    \pause

    \begin{alertblock}{}
     \textbf{Note:} We expect the same number of doubletons among 3 and 4 samples!
    \end{alertblock}

    \pause

    \begin{align*}
     \E[f_3] = \frac{2}{3}\E[t_2]\mu = \frac{2}{3}2N\mu = \frac{4N\mu}{3} = \frac{\theta}{3}
    \end{align*}


  \end{column}


 \end{columns}

\end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Site Frequency Spectrum (SFS)}
    \textbf{Expected SFS for $n$ samples (lineages)}\\
    \begin{columns}[T]
     \begin{column}{6cm}
      So far we found:
    \begin{itemize}
       \item $\E[f_1] =\theta$ among 2, 3 or 4 samples.
       \item $\E[f_2] =\frac{\theta}{2}$ among 3 or 4 samples.
       \item $\E[f_3] =\frac{\theta}{3}$ among 4 samples.
    \end{itemize}
     \end{column}
     \pause
     \begin{column}{3.8cm}
     \textbf{General rule}\\
    It is possible to prove that\\
     $\E[f_k] = \displaystyle \frac{\theta}{k}$
     \end{column}
    \end{columns}
\vspace{1cm}
       \pause
      \begin{columns}[T]
     \begin{column}{4.8cm}
     \textbf{Relative proportions}\\
    To get the expected relative proportions, we just have to divide by the total number of mutations:
    \begin{equation*}
     \frac{\E[f_k]}{\E[S]} = \displaystyle \frac{\displaystyle\frac{\theta}{k}}{\displaystyle \theta  \sum_{j=1}^{n-1}\frac{1}{j}} = \frac{\displaystyle \frac{1}{k}}{\displaystyle \sum_{j=1}^{n-1}\frac{1}{j}} = \frac{1}{\displaystyle k\sum_{j=1}^{n-1}\frac{1}{j}}
    \end{equation*}
     \end{column}
    \pause
    \begin{column}{5cm}
     \includegraphics[width=\columnwidth]{Figures/Expected_SFS.pdf}
    \end{column}
\end{columns}


\end{frame}
%-----------------------------------------------------------------------------------------
\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize}
    \item The probability of coalescence among $k$ lineages is ${k \choose 2}\frac{1}{2N}$.\medskip
    \item The total height of a genealogy of $n$ samples is expected to be $4N(1-\frac{1}{n})$.
    \item Trough most of the history of a genealogy there were only few lineages,\\
    through half of it there were actually only 2!
    \item The expected total length of a genealogy
    \begin{itemize}
    \item increases almost linearly with sample size.
    \item allows us to compute the expected number of mutations,
    \item and hence to estimate $\theta$ from the observed number of segregating sites.
    \end{itemize}
    \item The expected Site Frequency Spectrum (SFS) shows that most mutations are expected to be rare.
 \end{itemize}

 \pause
 \begin{block}{}
  For next week: please solve \textbf{Exercises 4}. Next week, we will simulate genealogies and look at them. If you want to reproduce these yourself, make sure you have \texttt{R} and \texttt{fastsimcoal} installed on your computer!
 \end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
