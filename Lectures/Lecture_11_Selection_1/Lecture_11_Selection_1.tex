\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Selection (deterministic)}

%TOPIC Deterministic selection in haploids
%TOPIC Deterministic selection in diploids
%READING Chapter 7 (pages 129 - 148)
%EXERCISES Exercises_7

\begin{document}
\maketitle

%TODO: it is a bit thin now: add calculation of s from data, as in book? More examples? Also, I did not discuss exercises about LD.

%-----------------------------------------------------------------------------------------
\section{Recap}

\hellodkjfk

\begin{frame}
\frametitle{Recap}

 \begin{itemize} 
       
   
    \item Loci on the same chromosome are linked, and so is there history.
    
    \item A measure of non-idependence of linked loci (the \textbf{Linkage-Disequilibrium}) is $D$, defined for each pair of alleles.
    \item $D$ evolves as a function of the recombination rate $c$.
    \item Importantly, one generation is \emph{not} enough to reach linkage-equilibrium, even if the loci are on different chromosomes!
    
 \end{itemize}
  
\end{frame}

   %------------------------------------------------------------------------
   \section{Selection in haploids}
 \begin{frame}
  \frametitle{Evolution of Drug Resistance in Influenza}
  \vspace{0.2cm}
 \includegraphics[width=\textwidth]{Figures/Influenza_Experiment.pdf}
 
 \pause
 \vspace{0.6cm}
 
 \begin{overlayarea}{\textwidth}{5cm}

\begin{onlyenv}<2|handout:0>\includegraphics[width=\textwidth]{influenza_trajectories/onlyNeutral.pdf}\end{onlyenv}
\begin{onlyenv}<3|handout:1>\includegraphics[width=\textwidth]{influenza_trajectories/withSelected.pdf} 
 \end{onlyenv}

  \end{overlayarea}
 
  
 \end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Simple model of selection}

\textbf{Individual Fitness}\\
The reproductive contribution of an individual to the next generation. Selection results as differences in fitness between individuals.\\\bigskip

\textbf{Selection acts on the Phenotype}\\
Selection can only act on phenotypic differences between individuals. A change in the phenotype over time only occurs if the phenotype is \emph{heritable}.\\\bigskip

\textbf{Genotypic Fitness $\lambda$}\\
To study the effect of selection on allele frequencies, we define the \emph{genotypic fitness} $\lambda$ as the average fitness of all individuals who have the same genotype.


\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Simple model of selection}

\textbf{A simple model for haploid selection}\\
\begin{itemize}
 \item $N$ haploid individuals, of which $N_A$ carry the $A$ and $N_a$ the $a$ allele.
 \item Each individual has the chance to survive and have offspring.
 \item On average, $A$-bearing individuals have $\lambda_A$ and $a$-bearing $\lambda_a$ descendants.
\end{itemize}

\pause
\medskip
\textbf{Allele frequency change due to selection}\\\smallskip
$f_A(0)=\displaystyle \frac{N_A}{N_A + N_a}$\\\bigskip

$f_A(1)=\displaystyle \frac{\lambda_A N_A}{\lambda_A N_A + \lambda_a N_a} = 
\displaystyle \frac{\lambda_A f_A(0)}{\lambda_A f_A(0) + \lambda_a f_a(0)} = \displaystyle \frac{f_A(0)}{f_A(0) + \displaystyle \frac{\lambda_a}{\lambda_A} f_a(0)} $\\\bigskip

\pause
\textbf{Note:} allele frequency changes depend on $\displaystyle \frac{\lambda_a}{\lambda_A}$, not the absolute values!

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Simple model of selection}

\textbf{Selection coefficient $s$ and relative fitness $\omega$}\\
Relative fitnesses: $\omega_A=1$ and $\omega_a = 1-s$, where $s$ is the selection coefficient.\\\medskip

Note that $\displaystyle \frac{\lambda_a}{\lambda_A} = \frac{\omega_a}{\omega_A}= 1-s$, and hence $f_A(1)= \displaystyle \frac{f_A(0)}{f_A(0) + (1-s) f_a(0)} $\\\medskip

\pause
\bigskip
\begin{columns}[T]
 \begin{column}{5cm}
\textbf{Change over multiple generations}\\
After $t$ time steps, we get:\\\bigskip
$f_A(t)= \displaystyle \frac{f_A(0)}{f_A(0) + (1-s)^t f_a(0)}$  

\pause
\bigskip

\textbf{Note:} the change is frequency is strongest when $f_A \approx \frac{1}{2}$.

\pause
\bigskip

\textbf{Rule of thumb:} it takes about $\frac{1}{s}$ generations for selection to change the allele frequency substantially.
 \end{column}
 \begin{column}{4.8cm} 
 \includegraphics<2->[width=\columnwidth]{Figures/expectedTrajectories.pdf}
 \end{column} 
\end{columns}


\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Selection versus Drift}

    
  \textbf{Confounding factor: genetic drift}\\
  This simple model of selection ignores  \emph{genetic drift}, but allele frequencies also change due to drift.\\\medskip
  \textbf{Question:} how to differentiate drift from selection?
  
  \pause
  \bigskip
  
  \textbf{We need a statistical test!}\\
  In order to decide if a locus under selection, we need to show that a model with selection is more likely than a model with drift alone.\\\bigskip
  
  \textbf{Frequentists example:} calculate the p-value as the probability to observe the data under drift alone. Reject drift model when $p < 0.05$.
  
  \pause\bigskip
  
  $\Rightarrow$ We will discuss this in detail over the next lectures!
  
  


\end{frame}

%------------------------------------------------------------------------
 {
     \usebackgroundtemplate{%
      \rule{0pt}{\paperheight}%
      \hspace*{0.975\paperwidth}%
      \makebox[0pt][r]{\includegraphics[width=0.95\paperwidth]{Figures/Influenza_results.png}}}
 
   \begin{frame}  
  \frametitle{Back to Influenza}

\begin{picture}(320,250)
\put(-10,230){\begin{minipage}[t]{0.45\linewidth}
{\textbf{Positions under selection}\\
  Shown are $\log p$ of how likely it is to obtain the observed trajectories under drift alone.}
\end{minipage}}
\end{picture}

%Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(0.7cm, 4cm)
    \raggedright  \footnotesize{Foll \textit{et al}. 2014} \hspace{.5em}
 \end{textblock*}}

\end{frame}
 }
 
 %------------------------------------------------------------------------
  \begin{frame}  
  \frametitle{Back to Influenza}
  
  \textbf{Bayesian Inference of Selection and Drift}\\
  Posterior distributions of effective population sizes ans selection coefficients for replicates with (oragnge) and without Oseltamivir (gray). \\
  Note that the virus also adapts to teh experimental conditions.
  
  \bigskip
  \bigskip 
  
  \includegraphics[width=\textwidth]{Figures/WFABC_estimates_Influenza.pdf}

  %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Kousathanas \textit{et al}. 2014} \hspace{.5em}
 \end{textblock*}}
\end{frame}
 
 
%------------------------------------------------------------------------
\section{Selection in diploids}

\begin{frame}
\frametitle{Selection in diploids}

\textbf{Selection in diploids}\\
A model of selection for diploid organisms is a combination of Hardy-Weinberg model of sexual reproduction and genotypic selection.\\\bigskip


Selection migth act at any point in the life cycle of an organism.\\\bigskip

\includegraphics[width=\textwidth]{Figures/whereSelectionActs.pdf}

\pause
\textbf{Viability selection}\\
Selection resulting from differences in individuals in their abilities to survive and reproduce (``Struggle for existence'' in Darwin's terms).\\\bigskip


\textbf{Note:} Viability selection is the easiest to work with, but the principles are the same for most kinds of selection.

\end{frame}
%------------------------------------------------------------------------
\begin{frame}
\frametitle{Viability selection: example}

\textbf{Coat color selection in Deer mice (\textit{Peromyscus maniculatus)}}\\
The Nebraska sand hills were formed $\approx$10,000 years ago (after last ice age).\\\medskip

Deer mice living in sand hills or the prairie differ in coat color.\\\medskip

Experiments with decoys: predation risk varies greatly with coat color.\\\medskip


\includegraphics[width=\textwidth]{Figures/Sandhill_mice.png}

%Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Linnen \textit{et al}. 2009} \hspace{.5em}
 \end{textblock*}}
\end{frame}

%------------------------------------------------------------------------

\begin{frame}
\frametitle{Viability selection: a simple model}

\textbf{Step 1: random mating}\\
If the allele frequencies among the parents are $f_A(t)$ and $f_a(t) = 1-f_A(t)$, the genotype frequencies among the zygotes are ${f_A(t)}^2$, $2f_A(t)f_a(t)$ and ${f_a(t)}^2$.\\\bigskip

\pause

\textbf{Step 2: Selection}\\
The probabilities of survival (viabilities) for the three genotypes are denoted by $\upsilon_{AA}$, $\upsilon_{Aa}$ and $\upsilon_{aa}$.\\\medskip

The allele frequencies in the next generation are then:

\begin{equation*}
 f_A(t+1) = \displaystyle\frac{\upsilon_{AA}{f_A(t)}^2 + \upsilon_{Aa}f_A(t)f_a(t)}{\upsilon_{AA}{f_A(t)}^2 + 2\upsilon_{Aa}f_A(t)f_a(t) + \upsilon_{aa}{f_a(t)}^2}% = \displaystyle \frac{\upsilon_{AA}{f_A(t)}^2 + \upsilon_{Aa}f_A(t)f_a(t)}{\bar{\upsilon}}
\end{equation*}\\\medskip

%where $\bar{\upsilon}$ is the average viability in the population.

\bigskip\pause
\textbf{Note:} again, only the relative viabilities matter!
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Viability selection: a simple model}

\textbf{Selection coefficients and relative fitness}\\
If we define $\omega_{AA} = 1$, $\omega_{Aa}=1-s_{Aa}$ and $\omega_{aa}=1-s_{aa}$, we get
\begin{equation*}
 f_A(t+1) =  \displaystyle \frac{{f_A(t)}^2 + (1-s_{Aa})f_A(t)f_a(t)}{{f_A(t)}^2 + 2(1-s_{Aa})f_A(t)f_a(t) + (1-s_{aa}){f_a(t)}^2}
\end{equation*}

\pause
\bigskip
\textbf{Special case: dominant advantageous allele}\\
Under a dominant model, $\omega_{AA} = \omega_{Aa}=1$ and $\omega_{aa}=1-s$, and hence
\begin{equation*}
f_A(t+1) =  \displaystyle \frac{{f_A(t)}^2 + f_A(t)f_a(t)}{{f_A(t)}^2 + 2f_A(t)f_a(t) + (1-s){f_a(t)}^2}% = \frac{{f_A(t)}^2 + f_A(t)f_a(t)}{\bar{\omega}}.
\end{equation*}

\pause
\bigskip

\textbf{Special case: recessive advantageous allele}\\
Under a recessive model, $\omega_{AA} = 1$ and $\omega_{Aa} = \omega_{aa}=1-s$, and hence
\begin{equation*}
f_A(t+1) =  \displaystyle \frac{{f_A(t)}^2 + (1-s)f_A(t)f_a(t)}{{f_A(t)}^2 + 2(1-s)f_A(t)f_a(t) + (1-s){f_a(t)}^2}% = \frac{{f_A(t)}^2 + (1-s)f_A(t)f_a(t)}{\bar{\omega}}.
\end{equation*}


\end{frame}


%------------------------------------------------------------------------
\begin{frame}
\frametitle{Viability selection: a simple model}

\textbf{Special case: genic selection}\\
If each copy of $a$ changes viability by the same factor, $\omega_{AA} = 1$, $\omega_{Aa} = 1-s$ and $\omega_{aa}=(1-s)^2$, we have 
\begin{equation*}
f_A(t+1) =  \displaystyle \frac{{f_A(t)}^2 + (1-s)f_A(t)f_a(t)}{{f_A(t)}^2 + 2(1-s)f_A(t)f_a(t) + (1-s)^2{f_a(t)}^2} = \frac{f_A(t)}{f_A(t) + (1-s)f_a(t)}
\end{equation*}

\textbf{Note:} in case of genic selection, a diploid is equivalent to a haploid population!

\bigskip\pause


\textbf{Special case: additive selection}\\
If each copy of $a$ reduces viability by the same amount, $\omega_{AA} = 1$, $\omega_{Aa} = 1-s$ and $\omega_{aa}=1-2s$, we have 
\begin{equation*}
f_A(t+1) =  \displaystyle \frac{{f_A(t)}^2 + (1-s)f_A(t)f_a(t)}{{f_A(t)}^2 + 2(1-s)f_A(t)f_a(t) + (1-2s){f_a(t)}^2} = \frac{f_A(t)-sf_Af_a}{1 - 2sf_a}
\end{equation*}

\textbf{Note:} since $(1-s)^2 \approx 1-2s$, additive selection = genic selection if $s$ is small!

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Directional selection}

\textbf{Directional selection}\\
Selection that is favoring one allele over another: $\upsilon_{aa} \leq \upsilon_{Aa} \leq \upsilon_{AA}$.\\
Example with $s_{aa} = 0.1$:\\\medskip
\includegraphics[width=\textwidth]{Figures/expectedTrajectoriesDiploid.pdf}

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Balancing selection}
\textbf{Balancing selection}\\
Selection that tends to favor polymorphism in the population. 
Example heterozygot advantage:  $\upsilon_{aa} \leq \upsilon:{Aa}$ and $\upsilon_{AA} \leq \upsilon_{AA}$.\\

\bigskip

\begin{columns}[T]
  \begin{column}{4cm}
   
   Equilibrium frequency is given by 
   
   \begin{equation*}
    \hat{f_A} = \displaystyle\frac{s_{aa}}{s_{AA} + s_{aa}}
   \end{equation*}

   Example: $s_{AA}=0.025$ and $s_{aa}=0.05$
   
   \begin{equation*}
    \hat{f_A} = \displaystyle\frac{0.05}{0.025+0.05} = \frac{0.05}{0.075} = \frac{2}{3}
   \end{equation*}
   
   
  \end{column}
  \begin{column}{5.8cm}
\includegraphics[width=\textwidth]{Figures/heterozygotAdvantage.pdf}  
  \end{column}

\end{columns}


\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Mutation Selection Balance}

Suppose a rare, recessive mutation $a$ with selection coeffcient $s_{aa}$ against this mutation. The frequency of homozygous individuals is $f_a^2$.\\\bigskip

\pause
Each generation, selection will reduce the frequency of $f_a$ by 

\begin{eqnarray*}
f_a(t+1) &=&  \displaystyle \frac{f_A(t)f_a(t) + (1-s_{aa}){f_a(t)}^2}{{f_A(t)}^2 + 2f_A(t)f_a(t) + (1-s_{aa}){f_a(t)}^2}\approx  f_A(t)f_a(t) + (1-s_{aa}){f_a(t)}^2\\ &=& f_a(t)(f_A(t) + f_a(t)) - s_{aa}f_a(t)^2 = f_a(t) - s_{aa}f_a(t)^2
\end{eqnarray*}

since $f_a \ll f_a$ and hence $f_A + 2f_A(t)f_a(t) + (1-s){f_a(t)}^2 \approx 1$.

Each generation, the frequency $f_a$ is increased by $\mu f_A \approx \mu$ due to new mutations.\\\bigskip

\pause
The change in frequency per generation is thus $\Delta f_a = \mu - s_{aa}f_a^2$.\\\bigskip

\pause
The equilibrium frequency of the mutation will then be $\hat{f_a} = \displaystyle \sqrt{\frac{\mu}{s_{aa}}}$





\end{frame}

%------------------------------------------------------------------------

\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize} 
       
    \item Selection may lead to more rapid changes in allele frequencies than expected under drift alone.
    
    \item While selection might act anywhere during the life cycle of an organism, we model \emph{viability selection} due to its simplicity.
    
    \item Allele frequency changes due to selection are driven by the relative fitness of the different genotypes.
    
    \item While dominant mutations increase the most quickly in frequency, they are the slowest to fix!
    
    \item Selection may also promote polymorphisms in the population, for instance through heterozygot advantage.
    
 \end{itemize}
 
 \pause
 \begin{block}{}
  For next week: solve \textbf{Exercises 8} and read \textbf{Chapter 8}.
 \end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
