
f <- function(t, f0=0.01, s=0.01){
  return(f0/(f0 + (1-s)^t*(1-f0)));
}

t <- 0:500

pdf("expectedTrajectories.pdf", width=3, height=3.25)
par(mar=c(3.5,3.5,0.5,0.75), las=1, mgp=c(2.5,1,0))
plot(0, type='n', xlim=c(0, max(t)), ylim=c(0,1), xaxs="i", yaxs="i", xlab="Generation", ylab=expression('Frequency f'[A]))
lines(t, f(t, s=0.02), col='red', lwd=2)
lines(t, f(t, s=0.01), col='orange', lwd=2)
lines(t, f(t, s=0.005), col='purple', lwd=2)
legend("topleft", legend=c("s=0.02", "s=0.01", "s=0.005"), col=c("red", "orange", "purple"), lwd=2, bty='n', cex=0.95)

dev.off()

#-----------------------------------------------------------------------------

f <- function(t, f0=0.025, sAA=0.0, sAa=0.01, saa=0.01){  
  r <- numeric(length(t));
  r[1] <- f0;
  for(i in 2:(length(t))){
    f0 = ((1-sAA)*f0*f0 + (1-sAa)*f0*(1-f0))/((1-sAA)*f0*f0 + 2*(1-sAa)*f0*(1-f0) + (1-saa)*(1-f0)*(1-f0));
    r[i] <- f0;
  }
  return(r);
}

t <- 0:500

pdf("expectedTrajectoriesDiploid.pdf", width=6, height=3.5)
par(mar=c(3.5,3.5,0.5,0.75), las=1, mgp=c(2.5,1,0))
plot(0, type='n', xlim=c(0, max(t)), ylim=c(0,1), xaxs="i", yaxs="i", xlab="Generation", ylab=expression('Frequency f'[A]))
lines(t, f(t, sAa=0.05, saa=0.1), col='black', lwd=2)
lines(t, f(t, sAa=0.1, saa=0.1), col='purple', lwd=2)
lines(t, f(t, sAa=0, saa=0.1), col='red', lwd=2)

legend("bottomright", legend=c("additive", "recessive", "dominant"), col=c("black", "purple", "red"), lwd=2, bty='n')

dev.off()



pdf("heterozygotAdvantage.pdf", width=4, height=3.5)
par(mar=c(3.5,3.5,0.5,0.75), las=1, mgp=c(2.5,1,0))
plot(0, type='n', xlim=c(0, max(t)), ylim=c(0,1), xaxs="i", yaxs="i", xlab="Generation", ylab=expression('Frequency f'[A]))
lines(t, f(t, f0=0.8, sAA=0.025, sAa=0.0, saa=0.05), col='black', lwd=2)
lines(t, f(t, f0=0.2, sAA=0.025, sAa=0.0, saa=0.05), col='purple', lwd=2)

dev.off()

