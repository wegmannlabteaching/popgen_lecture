

fst <- function(M){
  return (1/(1+8*M));
}

M <- seq(0, 10, length.out=100)

pdf("fst_vs_mig.pdf", width=6, height=4)
par(mar=c(4,4,0.1,0.1))
plot(M, fst(M), type='l', lwd=2, col='red', xlab="2Nm = Number of migrants", ylab="Fst")
dev.off();