\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Neutral Theory}

%TOPIC The neutral theory
%TOPIC Testing neutrality - evidencing selection
%READING Chapter 9 (179 - 191)
%EXERCISES Exercises_9

\begin{document}
\maketitle

%TODO: put Messer plot to SFS discussion

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Recap}

 \begin{itemize}

   \item Genetic drift adds randomness to the trajectories of selected mutations.

    \item The probability of fixation of a selected allele is affected by population size.

    \item Mutations for which $-1 < 2Ns < 1$ are considered \emph{nearly neutral} and are strongly affected by drift.

    \item The fixation probability of strongly advantageous alleles is low ($2s$)

    \item And even some deleterious alleles have a chance to get fixed!

    \item This leads to different substitution rates for selected and neutral sites, which allows to estimate the strength of selection from species comparisons.

    \item Neutral alleles linked to selected alleles may hitchhike to considerable frequencies.

 \end{itemize}

 \pause
 \bigskip

 \textbf{Question:} how common is selection?

\end{frame}

%------------------------------------------------------------------------
\section{The neutral Theory}
\begin{frame}
 \frametitle{The Neutral Theory}

 \textbf{The neo-Darwinian Synthesis}\\
 In the early 20th century, population genetics showed that evolution can be explained by natural selection acting on Mendelian genes.\\\bigskip

 \textbf{Large amount of genetic variation}\\
 When the first molecular data appeared, many people were surprised by the large amount of genetic variation in populations.\\\bigskip

 \textbf{Molecular Clock}\\
 The discovery of the molecular clock on amino acid sequences was discovered in 1962 by Zuckerhandl and Pauling.\\\bigskip




\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{The Neutral Theory}

\begin{columns}[T]
 \begin{column}{7.3cm}
  \textbf{Neutral Theory}\\
 Motoo Kimura realized that these findings can be explained by mutation and genetic drift alone. In 1968 he published the neutral theory of evolution.
 \end{column}
 \begin{column}{2.5cm}
  \includegraphics[width=\columnwidth]{Figures/Motoo_Kimura.jpg}
 \end{column}
\end{columns}

\bigskip



 \begin{block}{}
  \includegraphics[width=\textwidth]{Figures/kimura_1968.png}
 \end{block}


 \end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{The Neutral Theory}

 \textbf{The Neutral Theory}\begin{itemize}
	\item New mutations are either nearly neutral ($-1 < 2Ns < 1$) or very strongly deleterious ($2Ns \ll -1$).
	\item Advantageous mutations ($2Ns > 1$) are very rare.
	\item As a result, segregating mutations and fixed differences between species are neutral and only affected by genetic drift.
	\end{itemize}

 \bigskip

 \textbf{Is it true?}\\
 \pause
 The answer depends on the population size! In species like humans, most variation may well be neutral.\\ \bigskip

 \pause
 \textbf{But:} the most important contribution of the neutral theory is that it offers a null hypothesis to detect selection.




\end{frame}
%------------------------------------------------------------------------
\section{Tests of Neutrality}

\begin{frame}
 \frametitle{Conservation across species}

 \includegraphics[width=\textwidth]{Figures/lactase_conservation.pdf}

\end{frame}


%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Nonsynonymous vs synonymous rate (dN / dS)}

 \textbf{The influenza hemagglutinin}\\
  A surface protein of the Influenza virus required to dock to host cells.

 \begin{columns}[T]
 \begin{column}{4.1cm}
 \includegraphics[width=\columnwidth]{Figures/influenza_tree.png}
 \end{column}
 \begin{column}{4.3cm}


  \includegraphics[width=\columnwidth]{Figures/dnds_influenza_table.png}

 \end{column}

 \end{columns}
 
 % Citation
  \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Bush \textit{et al}. 1999} \hspace{.5em}
 \end{textblock*}}

\end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Nonsynonymous vs synonymous rate (dN / dS)}

 \textbf{Purifying (negative) selection in humans}\\

 \begin{columns}[T]
  \begin{column}{5cm}
  \includegraphics[width=\columnwidth]{Figures/dNdS_Europeans.pdf}
  \end{column}
  \begin{column}{4.8cm}
   \includegraphics[width=\columnwidth]{Figures/messer.png}
  \end{column}

 \end{columns}

% Citation
  \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Wegmann \textit{et al}. 2012} \hspace{.5em}
 \end{textblock*}}
 
\end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{HKA Test}

 \textbf{Limitations of dN / dS}\\
 Test based on comparing dN and dN are restricted to coding regions and only detect selection that persisted over a long time or affected many sites.\\\bigskip

 \textbf{Detecting recent selection}\\
 To detect recent selection, we need to consider variation within a species or population.\\\bigskip

 \textbf{The HKA test}\\
 The goal is to compare within species variation between different loci, while taking differences in mutation rates into account.\\\medskip




\end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{HKA Test}

 \textbf{Expected variation within a species}\\
 The expected number of segregating sites within a species is $E[S] = \displaystyle 4N\mu \sum_{i=1}^{n-1} \frac{1}{i}$\\\bigskip

 \textbf{Expected differences between species}\\
 Accounting for ancestral variation, the expected number of fixed differences between two species is $E[F] = 2T\mu + 4N_A\mu$\\\bigskip

 \textbf{Comparing genes}\\
 Under neutrality, we expect for genes 1 and 2:

 \begin{equation*}
  \frac{E[S_1]}{E[F_1]} = \displaystyle \frac{\displaystyle 4N\mu_1 \sum_{i=1}^{n-1} \frac{1}{i}}{2T\mu_1 + 4N_A\mu_1} = \frac{\displaystyle 2N \sum_{i=1}^{n-1} \frac{1}{i}}{T + 2N_A} = \frac{E[S_2]}{E[F_2]}
 \end{equation*}

 If the observed ratios $\frac{S_1}{F_1}$ and $\frac{S_2}{F_2}$ are different, selection likely acted on one locus.



\end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Effect of selection on SFS}

  \centering
 \includegraphics[width=0.8\columnwidth]{Figures/selection_on_sfs.png}

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Tajima's $D$}

 \textbf{Tajima's $D$}\\
 Fumio Tajima proposed a test based on the distribution of allele frequencies.\\\medskip

 Under neutrality, we have two unbiased estimators of $\theta=4N\mu$:

 \begin{equation*}
  \hat{\theta}_T = \frac{\sum_{i=1}^{n-1}\sum_{j=i+1}^n d_{ij}}{n(n-1)/2} \quad \quad \mbox{and} \quad \quad  \hat{\theta}_W = \frac{S}{\sum_{i=1}^{n-1}\frac{1}{i}}
 \end{equation*}

  Tajima's $D$ is defined as \\\medskip

  $D = \displaystyle \frac{\hat{\theta}_T - \hat{\theta}_W}{\sqrt{\hat{V}(\hat{\theta}_T - \hat{\theta}_W)}}$, where $\hat{V}(\hat{\theta}_T - \hat{\theta}_W)$ the variance in $\hat{\theta}_T - \hat{\theta}_W$

 \pause
 \bigskip
 \textbf{Why does this work?}\\
 Let's look at some genealogies on the board ...

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{\textit{FOXP2} - the evolution of speech?}

 \begin{overlayarea}{\textwidth}{\textheight}

  \textbf{The \textit{FOXP2} gene}\begin{itemize}
 \item Humans with mutations in the \textit{FOXP2} gene are healthy, but have difficulties to form gramatically correct sentences.
 \item \textit{FOXP2} is highly conserved among most mammals, but humans differ by two amino acids.
 \end{itemize}
 \medskip

 \begin{onlyenv}<1|handout:0>
 \includegraphics<1>[width=\columnwidth]{Figures/foxp2_tree.pdf}
 \end{onlyenv}
\begin{onlyenv}<2|handout:1>
 \includegraphics<2>[width=0.8\columnwidth]{Figures/foxp2_res.png}
\end{onlyenv}
\end{overlayarea}

% Citation
  \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Enard \textit{et al}. 2002} \hspace{.5em}
 \end{textblock*}}

\end{frame}
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Demography is a confounding factor}


 \begin{columns}[T]
  \begin{column}{5.5cm}
 \includegraphics[width=\columnwidth]{Figures/sfs_europeans.pdf}
  \end{column}
  \begin{column}{5cm}
  \textbf{The effect of demography}\\
  As we have seen, demography affects the SFS and hence Tajimas'D (and all other statistics) as well!
    \end{column}

 \end{columns}
  \bigskip
  \pause

  It is thus required to either

  \begin{itemize}
  \item compare neutrality statistics cross genes or functional categories
  \item or to use coalescent simulations to check if a specific value of a statistics is not compatible with neutrality!
  \end{itemize}

  % Citation
  \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Wegmann \textit{et al}. 2012} \hspace{.5em}
 \end{textblock*}}
 

\end{frame}

%------------------------------------------------------------------------
\begin{frame}
\frametitle{Detecting selection from Time Series Data} 
 
 \textbf{The Bronze Age Battle at Tollense}
   \begin{itemize}
    \item Massive battle around 1250 BC involving 2,000-6,000 combatants.
    \item Oldest archaeological site of a battlefield of this size in Europe.
    \end{itemize}
    
    \textbf{Genomic data for 18 soldiers}
   \begin{itemize}
    \item 5,000 unlinked, putatively neutral regions of 1000bp each (5 Mb)
    \item 438 phenotypically informative markers
   \end{itemize}
   
   \bigskip
   
    \includegraphics[width=\columnwidth]{Figures/Tollense_bones_combined.png}    
   
   \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Burger \textit{et al}. 2020} \hspace{.5em}
 \end{textblock*}}   
   
\end{frame}
%------------------------------------------------------------------------


\begin{frame}
 \frametitle{Detecting selection from Time Series Data} 

 
\begin{columns}[T]

 \begin{column}{5.8cm}
 
 \textbf{Where were these soldiers from?}\\
 $F_{ST}$ measures of genetic distance.
 
 \end{column}
\begin{column}{5.0cm}

\textbf{One or two populations?}\\
 $F$ measures population structure.
 
 
 \end{column}
 \end{columns}
 
  \bigskip
 
  \begin{columns}[T]
  \begin{column}{5.8cm}   
    \includegraphics[width=\columnwidth]{Figures/Tollense_FST.jpg}

\end{column}
 \begin{column}{5cm}
     \includegraphics[width=\columnwidth]{Figures/Tollense_posteriors_simple.pdf}
  
 \end{column}
 \end{columns}
 
 \bigskip
 
   \begin{columns}[T]
  \begin{column}{6.0cm}   
  \begin{center}
   \textbf{Northern Europeans}
  \end{center}  
\end{column}
 \begin{column}{5cm}
 \begin{center}
  \textbf{One population}
 \end{center}
  
 \end{column}
 \end{columns}
 
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Burger \textit{et al}. 2020} \hspace{.5em}
 \end{textblock*}}
 
 \end{frame}

%---------------------------------------------------------------------------------------------------------------------

 \begin{frame}[t]
\frametitle{Detecting selection from Time Series Data} 

 \begin{columns}[T]
  \begin{column}{6cm}   
    \textbf{438 markers with known association}\\
    \begin{itemize}
     \item visible phenotypes\\(e.g. pigmentation, morphology)
     \item metabolism\\(e.g. lactose and alcohol digestion)
     \item medical conditions\\(e.g. diabetis risk)
    \end{itemize}
    \vspace{0.2cm}
    1000 Genomes CEU as modern reference.
    
    \vspace{0.5cm}
    \textbf{Strong selection on Lactase expression}\\
    \begin{itemize}
     \item Frequency of rs4988235: 2/32 $\rightarrow$ 144/192.
    \end{itemize}
    
 \end{column}
 
 


 \begin{column}{5cm}
    \includegraphics[width=\columnwidth]{Figures/Selection_tollense_combined.pdf}
 \end{column}

\end{columns}


\onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Burger \textit{et al}. 2020} \hspace{.5em}
 \end{textblock*}}

  \end{frame}
  
%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Detecting selection based in population differentiation}

 \textbf{Elevated $F_{ST}$ in the \textit{EPAS1} gene}\\\medskip
 \includegraphics[width=\columnwidth]{Figures/fst_epas1.png}


 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Huerta-S\'anchez \textit{et al}. 2014} \hspace{.5em}
 \end{textblock*}}
 
\end{frame}

%------------------------------------------------------------------------
\begin{frame}
 \frametitle{Detecting selection based in population differentiation}

 \textbf{Elevated $F_{ST}$ in the \textit{EPAS1} gene}\\\medskip
 \includegraphics[width=0.8\columnwidth]{Figures/haplotypes_epas1.png}

 %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Huerta-S\'anchez \textit{et al}. 2014} \hspace{.5em}
 \end{textblock*}}
 
\end{frame}

%------------------------------------------------------------------------

\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize}

    \item The neutral theory postulates that most segregating and fixed mutations are nearly neutral.

    \item While selection has been evidenced in many organisms, most of the genome may well be neutral in many species with small to medium $N_e$.

    \item The neutral theory serves as a null hypothesis that can be tested.

    \item Comparing nonsynonymous vs synonymous mutations between species is powerful in detecting selection acting over longer times.

    \item HKA, Tajima's $D$ and similar tests allow us to detect selection that has been acting recently.

    \item Demography is a major confounding factor that can not be ignored!

    \item Tests based on population differentiation are more robust to demography, but can only detect divergent and balancing selection.

 \end{itemize}

 %\pause
 %\begin{block}{}
   %For next week, solve \textbf{exercises 9.1, 9.2, 9.3 \& 9.4}.
 %\end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
