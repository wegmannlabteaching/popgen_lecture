#!/bin/bash
#compile all lectures as both presentation and handout
#And write an HTML table for moodle with links

#course settings
startDate="Sep 17 2020"
recompile="true"
cleanup="true"

#prepare html output
html=`pwd`"/content.html"
echo "" > ${html}

solutions="<h4>Solutions to Exercises</h4><ul>"

#for num in `seq 1 1 14`
for num in 8
do
	lecture=$(ls -d Lecture_${num}_*)
	echo -n "Processing lecture '$lecture' ..."
	if [ -d ${lecture}  ]
	then 
		cd $lecture
		
		#date and name
		weekDiff=$(echo "$num - 1" | bc)
		date=$(date '+%B %-d %Y' -d "${startDate} + ${weekDiff} weeks")
		name=$(echo ${lecture} | sed 's/_/ /g' | cut -d' ' -f 3-99)
		echo "<h4>${date}: ${name}</h4>" >> ${html}
		
		if [ -f ${lecture}.tex ]
		then 	
			handoutName=${lecture}_handout.pdf
			if [ $recompile = "true" ]; then
				#compile handout

				sed -i 's/documentclass\[9pt\]{beamer}/documentclass\[9pt,handout\]{beamer}/' ${lecture}.tex
				pdflatex -file-line-error -interaction nonstopmode ${lecture}.tex > /dev/null
				mv ${lecture}.pdf ${handoutName}
				
				#compile lecture
				sed -i 's/documentclass\[9pt,handout\]{beamer}/documentclass\[9pt\]{beamer}/' ${lecture}.tex
				pdflatex -file-line-error -interaction nonstopmode ${lecture}.tex > /dev/null
			fi
			
			#set tex file as source for content information
			contentFile=${lecture}.tex
		else
			handoutName=""
			#read content from ${lecture}.content
			contentFile=${lecture}.content		
		fi
		
		#clean up latex output
		if [ $cleanup = "true" ]; then
			rm *.aux *.log *.toc *.nav *.snm *.out
		fi
			
		#process content of lecture
		#extract topics
		topics=$(awk '$1=="%TOPIC"{print $0 "</li>"}' ${contentFile} | sed 's/%TOPIC /<li>/')
		if [ -n "$topics" ]; then
			echo "<p><b>Topics:</b><ul>${topics}</ul></p>" >> ${html}
		fi
		
		if [ -n "handoutName" ]; then
			echo "<p><b>Handout</b>: <a href=\"https://bitbucket.org/wegmannlab/popgen_lecture/raw/master/Lectures/${lecture}/${handoutName}\" target=\"_blank\">${handoutName}</a></p>" >> ${html}
		fi
		
		#extract readings
		reading=$(awk '$1=="%READING"{print $0}' ${contentFile} | sed 's/%READING //')
		if [ -n "$reading" ]; then
			reading=$(echo "$reading" | sed ':a;N;$!ba;s/\n/ and /g')
			echo "<p><b>Reading:</b> Please read ${reading} prior to the lecture.</p>" >> ${html}
		fi
		
		#extract exercises
		exercises=$(awk '$1=="%EXERCISES"{print $0}' ${contentFile} | sed 's/%EXERCISES //')				
		if [ -n "$exercises" ]; then			
			solutionLinks=$(for e in $exercises; do echo "<li><a href=\"https://bitbucket.org/wegmannlab/popgen_lecture/raw/master/Exercises/${e}/${e}_solutions.pdf\" target=\"_blank\">${e}_solutions.pdf</a></li>"; done)
			solutions="${solutions}${solutionLinks}"
			exercises=$(before=""; for e in $exercises; do echo "${before}<a href=\"https://bitbucket.org/wegmannlab/popgen_lecture/raw/master/Exercises/${e}/${e}.pdf\" target=\"_blank\">${e}.pdf</a>"; before=" and "; done)			
			echo "<p><b>Exercises:</b> We will discuss ${exercises} in class. You will benefit most by solving them before.</p>" >> ${html}
		fi
		
		#extract scripts
		if [ `ls Scripts/* 2> /dev/null | wc -l` -gt 0 ]; then
			scripts=$(before=""; for s in Scripts/*.r; do scriptName=$(echo $s | sed 's#Scripts/##'); echo "${before}<a href=\"https://bitbucket.org/wegmannlab/popgen_lecture/raw/master/Lectures/${lecture}/${s}\" target=\"_blank\">${scriptName}</a>"; done)
			if [ -n "$scripts" ]; then			
				echo "<p><b>Scripts:</b> We will use ${scripts} to demonstrate some concepts.</p>" >> ${html}
			fi
		fi
			
		#exit lecture folder
		cd ..
	fi
	echo " done!"
done

#add solutions
echo "${solutions}</ul>" >> ${html}

