#Posterior prob of assignment of random individuals
assignprob <- function(nInd, nLoci, f1=0.48, f2=0.52){
  #simulate individuals from pop1
  d <- matrix(rbinom(nInd*nLoci, size=2, prob=f1), nrow=nInd);
  
  #calc post prob
  p1 <- apply(pbinom(d, size=2, prob=f1), 1, prod)
  p2 <- apply(pbinom(d, size=2, prob=f2), 1, prod)
  B <- p1/(p1 +p2);
  return(B);
}

#make boxplot as function of num Loci
L <-1:100
nInd <- 50;
d <- matrix(0, ncol=length(L), nrow=nInd);
for(i in 1:length(L)){
  d[,i] <- assignprob(nInd, L[i]);
}

q <- apply(d, 2, quantile, probs=c(0.05, 0.5, 0.95))

#plot
pdf("Assignprob_small_freq_diff.pdf", height=3, width=3)
par(mfrow=c(1,1), oma=c(0,0,0,0), mar=c(3.5,4,0.5,1), las=1, mgp=c(3,1,0))
plot(0, type='n', ylim=c(0.5, 1), xlim=c(0, max(L)), xaxs="i", yaxs='i', xlab="", ylab="P(Pop=1|G)", xaxt='n');
axis(1, at=c(0,25,50,75,100))
mtext("Number of Loci", 1, line=2)
x <- c(L, rev(L), L[1]);
y <- c(q[1,], rev(q[3,]), q[1,1]);

polygon(x,y, col="lightblue", border=NA);
lines(q[2,], lty=1, lwd=2, col="blue")
lines(q[1,], lty=2, col="blue")
lines(q[3,], lty=2, col="blue")
dev.off()



