\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Inferring Population Histories}

%TOPIC Inferring evolutionary histories using th likelihood function
%TOPIC Examples: Population assignment and estimating phylogenetic trees
%TOPIC Gene Trees versus Species Trees
%TOPIC The Felsenstein equation and examples
%READING Chapter 5 (pages 77 - 92)
%EXERCISES Exercises_5

\begin{document}
\maketitle

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Recap}

  \begin{itemize} 
    \item Many natural populations are structured or subdivided.
    \item When ignoring such divisions, samples taken from pooled populations show a deficit of heterozygots (Wahlund effect).
    \item A common measure of subdivisions is $F_{ST}$, which is 0 when allele frequencies are identical between populations and 1 if the populations are fixed for different alleles.  
    \item In contrast to Chimpanzees, Humans show only little structure, even across continents.
    \item Using coalescent theory, it is not that hard to derive the expected $F_{ST}$ values under both migration and divergence models.
    \item This allows us to infer migration rates or divergence times from contemporary samples.
    \item However, $F_{ST}$ alone does not allow us to distinguish between migration and divergence (or combination of the two)!
 \end{itemize}
 
 \pause
 \bigskip
 
 \textbf{Question:} How can we infer more complex demographic histories?\\\bigskip
 
  
\end{frame}

%-----------------------------------------------------------------------------------------
 \section{Population Assignment}

  \begin{frame}
  \frametitle{Where does Ivory come from?}
 
  \begin{columns}
   \begin{column}{5cm}
      \textbf{Ivory trade}\\
  Every year, several ten-thousand elephants are killed for their ivory.\\\medskip
  To efficiently fight poaching, it is important to know where seized ivory comes from.\\
    \end{column}
   \begin{column}{5cm}
    \includegraphics[width=\columnwidth]{Figures/kws-against-ivory-poaching.jpg}
   \end{column}
  \end{columns}

  \pause
  \bigskip\bigskip
  \textbf{Question:} How can population genetics help?
  
 \end{frame} 
 
 %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Population Assignment}
  
  \textbf{Population Assignment}\\
  Inferring the population of origin of a sample from genetic data.\\\bigskip
  
  \textbf{Statistical Inference}\\
  Drawing conclusions from data that is subject to random variation.\\\bigskip
  
  \textbf{The Likelihood Function}\\
  The probability of the data ${\cal D}$ given the parameters of the model $\Theta$: $\P({\cal D}|\Theta)$\\\bigskip
      
   \pause
      
  \begin{columns}[T]
   \begin{column}{5.8cm}
  \textbf{Example: 2 populations, 1 locus}\\
  What is the probability that an individual $g=AA$ (the data ${\cal D}$) originates from population $P=1$ (the parameter $\Theta$)?\\\medskip
  
  
  \onslide<3>{Assuming HWE (the model): $\P(g=AA|P=1) = {f_1}^2$}
  
   \end{column}
\begin{column}{4cm}
 \includegraphics<2->[width=\columnwidth]{Figures/Pop_Assignmenet_example.pdf}  
\end{column}
\end{columns}
    
 \end{frame}

  %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Maximum Likelihood}
  \textbf{Maximum Likelihood Inference}\\
  The maximum likelihood estimate of a parameter is the value for which the likelihood is maximized.\\\bigskip
  
  \onslide<2->{\begin{columns}[T]
   \begin{column}{5.8cm}
  \textbf{Example: 2 populations, 1 locus}
  \begin{align*}
   \P(g=AA|P=1) &= {f_1}^2= 0.2^2 = 0.04\\
   \P(g=AA|P=2) &= {f_2}^2= 0.8^2 = 0.64
  \end{align*}
  }

   \end{column}
    
   \onslide<2->{
\begin{column}{4cm}
 \includegraphics<2->[width=\columnwidth]{Figures/Pop_Assignmenet_example.pdf}  
\end{column}}
\end{columns}
  
  \bigskip\bigskip
    \onslide<3>The Maximum Likelihood Estimate is therefore: $\hat{P}_{MLE}=2$
  
 \end{frame}
  %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Bayesian Inference}
  
  \textbf{Bayesian Statistics}\\
  In Bayesian statistics, the goal is to infer the \emph{posterior probability}, the probability of the parameters $\Theta$ given the data ${\cal D}$: $\P(\Theta|{\cal D})$\\\bigskip
  
  \textbf{Bayes' Rule}\\
  According to probability theory,\\
  \begin{equation*}
    \P(\Theta|{\cal D}) = \displaystyle\frac{\P({\cal D}|\Theta)\P(\Theta)}{\P({\cal D})} =    \displaystyle\frac{\P({\cal D}|\Theta)\P(\Theta)}{\int_{\Theta} \P({\cal D}|\Theta)\P(\Theta) d_\Theta}
  \end{equation*}  


  Here,\begin{itemize}
        \item $\P(\Theta)$ is the \textbf{prior} probability, the probability of the parameter \emph{before} looking at the data (yes, this is subjective!).
        \item $\P(\Theta|{\cal D})$ is the \textbf{posterior} probability of the parameter \emph{after} considering the data.
  \end{itemize}

  
 \end{frame}
  %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Bayesian Inference}
 
 \textbf{Example: 2 populations, 1 locus}
  \begin{eqnarray*}
   \P(P=1|g=AA) &=& \frac{\P(AA|P=1)\P(P=1)}{\P(g=AA)}\\
   &=& \frac{\P(g=AA|P=1)\P(P=1)}{\P(g=AA|P=1)\P(P=1) + \P(g=AA|P=2)\P(P=2)}\\
  \end{eqnarray*}
  
  If we do not have any additional information regarding the population the individual is coming from, we set $\P(P=1)=\P(P=2)=\frac{1}{2}.$ \vspace{-0.4cm}
  
    \begin{eqnarray*}
   \P(P=1|g=AA) &=& \displaystyle \frac{\frac{1}{2}{f_1}^2}{\frac{1}{2}{f_1}^2+\frac{1}{2}{f_2}^2} = \frac{{f_1}^2}{{f_1}^2+{f_2}^2}\\ \\\pause
   &=& \frac{0.04}{0.04 + 0.64} = \frac{0.04}{0.68} = 0.059
  \end{eqnarray*}
  
  And hence $\P(P=2|g=AA) = 0.941$
  
  Our Bayesian estimate thus suggest the individual is coming from population 2.
 
 \end{frame}
  %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Multiple Loci}
  
  \textbf{Small allele frequency differences}\\
  Often, allele frequencies are very similar between populations, which is limiting the power of population assignment.\\\medskip
  \textbf{Example:} if $f_1=0.48$ and $f_2=0.52$, $\P(P=1|AA) = 0.460$\\\bigskip
  
  \pause
   \begin{columns}[T]
   \begin{column}{5cm}
      \textbf{Incorporating multiple loci}\\
      Assuming all loci to be independent, 
      \begin{align*}
             &\P(g_1, \ldots, g_L|Pop)=\displaystyle\prod_{i=1}^L \P(g_i|Pop)                              
      \end{align*}

      \pause
      
      \textbf{Example:} Median and 90\% quantile of $\P(g_1, \ldots, g_L|Pop)$ as a function of the number of loci for random individuals from population 1 with $f_1=0.45$ and $f_2=0.55$
  
   \end{column}

    \begin{column}{4.8cm}
     \includegraphics[width=\columnwidth]{Figures/Assignprob_small_freq_diff.pdf}
    \end{column}


  \end{columns}
 \end{frame}
  %------------------------------------------------------------------------
 {
     \usebackgroundtemplate{%
      \rule{0pt}{\paperheight}%
      \hspace*{\paperwidth}%
      \makebox[0pt][r]{\includegraphics[width=\paperwidth]{Figures/elephant_ok.jpg}}}
 
   \begin{frame}
  
  \frametitle{Where does Ivory come from?}

 \end{frame}
 }
  %------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Admixture}
  
  \textbf{Admixture}\\
  Admixed individuals carry genetic ancestry from multiple, diverged populations.\\\smallskip
  
    \begin{columns}[T]
   \begin{column}{6.1cm}
   
  \textbf{Inferring Admixture}\\
  \begin{itemize}
                        \item The allele frequencies at each locus in each population.
                        \item The proportion of each individual's genome originating from each population.
                       \end{itemize}
   \end{column}
\begin{column}{3.7cm}
 \includegraphics[width=\columnwidth]{Figures/Admixture_example.pdf}  
\end{column}
\end{columns}

  \bigskip
  \pause
  
  \textbf{The Likelihood Function (under HWE)}\\
  For a single locus with alleles $A$ and $C$, for instance,\vspace{-0.2cm}
    \begin{eqnarray*}
   \P(g=AA|f_1, f_2, \alpha) &=& \left[\alpha f_1 + (1-\alpha) f_2\right]^2\\
   \P(g=AC|f_1, f_2, \alpha) &=& 2\left[\alpha f_1 + (1-\alpha) f_2\right]\left[\alpha (1-f_1) + (1-\alpha) (1-f_2)\right] 
  \end{eqnarray*}
   
  \pause
  
  Assuming individuals and loci to be independent, the total likelihood is simply the product across all individuals and loci.
    
 \end{frame}
 
  %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Admixture}
  \textbf{Example: Admixture among Afro-Caribbeans}\\
  YRI: Yoruba (Nigeria); MEX: Mexicans; CEU: Europeans.\\\bigskip

  
   
  \makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{Figures/ancestryplot_afrocarribeans.pdf}}%
  
 \end{frame}
  %------------------------------------------------------------------------
 \section{Phylogenetics}
 \begin{frame}
  \frametitle{Phylogenetics}
  

      
  \begin{columns}[T]
   \begin{column}{6.7cm}
   
     \textbf{Phylogenetics}\\
  Inference of evolutionary trees from genetic data. \\\bigskip
   
   \textbf{Likelihood function}\\
  $\P(\mbox{DNA Sequences}| \mbox{Tree}, \mbox{Mutation model})$\\\bigskip
  


     \textbf{Example: Human Evolution}\\
  
 \texttt{Human \ : aagacacaga gatagac\textcolor{red}{c}ag}\\
 \texttt{Chimp \ : aagac\textcolor{red}{g}caga gatagac\textcolor{red}{c}ag}\\
 \texttt{Gorilla: aagacacaga \textcolor{red}{t}atagacaag}\\\bigskip
 
 

   \end{column}
  \begin{column}{3.1cm}
  \includegraphics[width=\columnwidth]{Figures/Human_chimp_gorilla_trees.pdf}
   
\end{column}
\end{columns}
    
  
 \end{frame}
   %------------------------------------------------------------------------
   \begin{frame}
  \frametitle{Human mtDNA tree}
  \vspace{-0.2cm}
 \makebox[\textwidth][c]{\includegraphics[width=1.18\textwidth]{Figures/mtDNA_human.jpg}}%
 
 
 
 \end{frame}
   %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Gene Trees vs Species Trees}
      
  \begin{columns}[T]
   \begin{column}{6.2cm}
   
   \textbf{Overestimating Divergence Times}\\
   We estimate coalescent times (not divergence times): since $t>T$, we tend to overestimate divergence times.\\\medskip
   
   This is particularly pronounced if $T$ is small and $N$ is large.\\\bigskip
   

   \end{column}
\begin{column}{3.6cm}
  \includegraphics[width=\columnwidth]{Figures/Reciprocal_monophyly.pdf}  
  
\end{column}
\end{columns}

\pause\bigskip
    
      \begin{columns}[T]
   \begin{column}{6.2cm}
   
   \textbf{Incomplete Lineage Sorting}\\
   If $T$ is small or $N$ large, the coalescent tree may not match the population / species tree.\\\bigskip
   
   \onslide<3>{
   \textbf{Note:} not all gene trees match the species tree! To estimate trees among young species, we thus need many independent loci.
   }

   \end{column}
\begin{column}{3.6cm}
  \includegraphics[width=\columnwidth]{Figures/Incomplete_lineage_sorting.pdf}
  
  
\end{column}
\end{columns}
  
  
 \end{frame}
   %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Gene Trees vs Species Trees}
  
  \textbf{Gene Tree vs. Species Tree}\\
   \begin{columns}[T]
   \begin{column}{6.0cm}  
   The human and chimp lineages do not coalesce in the human-chimp ancestral species with probability\\ \medskip
   $\P(\mbox{no coal}|T_{HC}, N_{HC})=\displaystyle{\left(1-\frac{1}{2N_{HC}}\right)}^{T_{HC}}$\\\medskip
      
   If they do not coalesce, $\frac{2}{3}$ of all gene trees will be either ((H,G),C) or ((G,C),H)!
   
   \end{column}
\begin{column}{3.8cm}
  \includegraphics[width=\columnwidth]{Figures/Gene_vs_species_tree_humans.pdf}
  
 
\end{column}
\end{columns}
\bigskip
     We thus have:  $\P(T\neq((H,C),G)|T_{HC}, N_{HC})=\displaystyle\frac{2}{3}{\left(1-\frac{1}{2N_{HC}}\right)}^{T_{HC}}$\\\medskip
   
   \pause
   \textbf{Looking at data}\\
   $\frac{2}{3}$ of all gene trees are ((H,C),G), and $\frac{1}{6}$ are ((H,G),C) and ((G,C),H), each.\\\medskip
   
   This suggests that either $T_{HC}$ was rather short or $N_{HC}$ very large.\\ Fossil data suggests the former.
   
  
 \end{frame}
   %------------------------------------------------------------------------
   \section{Felsenstein Equation}
 \begin{frame}
  \frametitle{The Felsenstein Equation}
  
  \textbf{Inferring demographic parameters}\\
  Since interpreting single trees is difficult, we would like to infer demographic parameters without having to infer evolutionary trees or genealogies.\\\medskip
  
  \pause
  
  Using coalescent theory, we have seen how to calculate $\P(G|\Theta)$, the probability of a genealogy given demographic parameters $\Theta$.\\\medskip
  
  \pause
  
  Using a mutation model $\mu$, we have seen today how to calculate $\P({\cal D}|G, \mu)$, the probability of DNA sequence data ${\cal D}$ given a genealogy $G$.\\\medskip
  
  \pause
  \textbf{The Felsenstein Equation}\\
  If we want to calculate $\P({\cal D}|\Theta)$, we have to calculate $\P({\cal D}|G, \mu)$ for \emph{all possible genealogies}, and weight them by $\P(G|\Theta)$:
  
  \begin{equation*}
   \P({\cal D}|\Theta, \mu) = \int_{G}\P({\cal D}|G, \mu)\P(G|\Theta) dG
  \end{equation*}

  
 \end{frame}
   %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{The Felsenstein Equation}
  
  \textbf{The Felsenstein Equation}\\
  \begin{equation*}
   \P({\cal D}|\Theta, \mu) = \int_{G}\P({\cal D}|G, \mu)\P(G|\Theta) dG
  \end{equation*}\bigskip
  
  \textbf{The Felsenstein Equation in practice}\\
  Unfortunately, this integral is impossible to solve analytically in all but some extremely simple models.\\\medskip
  
  \pause
  
  In practice, we thus often approximate this integral using a random sample of coalescent trees from $\P(G|\Theta)$.
  
  \begin{equation*}
   \P({\cal D}|\Theta, \mu) \approx \frac{1}{N}\sum_{i=1}^N\P({\cal D}|G_i, \mu) \mbox{\hspace{10pt} where \hspace{10pt}} g_i \sim \P(G|\Theta)
  \end{equation*}\bigskip
  
 \end{frame}
 %-----------------------------------------------------------------------------------------
\begin{frame}
 \frametitle{Cross River Gorilla}

 
 \makebox[\textwidth][c]{\includegraphics[width=1.15\textwidth]{Figures/gorilla_results.pdf}}%
  
   %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Thalmann \textit{et al}. 2011} \hspace{.5em}
 \end{textblock*}}

\end{frame}
   %------------------------------------------------------------------------
 \begin{frame}
  \frametitle{Demographic history of Mbuti Pygmies}
  
\begin{overlayarea}{\textwidth}{\textheight}
\vspace{1cm}
     \begin{columns}[T]
   \begin{column}{7.1cm}  
   
  
   \begin{onlyenv}<1|handout:0>{\hspace{1.75cm} \includegraphics[width=0.5\columnwidth]{Figures/IMG_2033.jpg}}\end{onlyenv}  
  
\begin{onlyenv}<2|handout:1>\includegraphics[width=\columnwidth]{Figures/hgdp_figure_mbuti.pdf}\end{onlyenv}

  
   \end{column}
\begin{column}{4.6cm}
  \includegraphics[width=\columnwidth]{Figures/3-pop-model.pdf}
  
  
\end{column}
\end{columns}
  \end{overlayarea}
  
  %Citation
 \onslide<1->{
 \begin{textblock*}{\paperwidth}(1cm,\textheight + 0.5cm)
    \raggedright  \footnotesize{Wegmann \textit{et al}. 2009} \hspace{.5em}
 \end{textblock*}}
 \end{frame}
%------------------------------------------------------------------------
\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize} 
    \item The basis of most inference techniques is the \textbf{Likelihood function}.
    \item We have seen two inference techniques: \textbf{Maximum Likelihood} and \textbf{Bayesian Estimation}
    \item These techniques can be used to assign individuals to populations or to infer evolutionary trees.
    
    \item Inferred trees are \emph{coalescent trees}, which may differ from population or species trees due to \textbf{incomplete lineage sorting}.
    
    \item Evolutionary histories should thus be inferred from multiple / many loci.
    
    \item To infer demographic parameters, we need to integrate over all possible coalescent trees (\textbf{Felsenstein Equation}).
    
    \item This is hard to do analytically, but possible to do numerically using coalescent simulations.
    
 \end{itemize}
 
 \pause
 \begin{block}{}
  For next week: please solve \textbf{Exercises 6} and read \\the rest of \textbf{Chapter 5} (pages 92 - 103) as well as \textbf{chapter 6}.
 \end{block}

\end{frame}
%-----------------------------------------------------------------------------------------

\end{document}
