
Ne<-function(t, N=1, r=log(2)){
  t<-t+1
  x<-(1-1/exp(r*t))/(1-1/exp(r));  
  return(N*t/x);
}
N<-function(t, N=1, r=log(2)){
  return(N*exp(r*t));
}


pdf("effective_size_growth.pdf", width=4, height=5)
par(las=1, oma=c(0,0,0,0), mar=c(4,4,0.1,0.1));
t<-0:50
plot(0, type='n', ylim=c(0,12), xlim=c(0, max(t)), xlab="Generations", ylab="log10(population size)", xaxt='n')

lines(t, log10(Ne(t)), col='red', lwd=3);
lines(t, log10(N(t)), col='black', lwd=3);

y<-log10(Ne(max(t)));
lines(par("usr")[1:2], rep(y, 2), lty=2, col='red');
axis(side=2, at=y, labels=round(y, digits=4), col='red');


l<-seq(0, max(t), by=10)
axis(side=1, at=l, labels=l);
legend('topleft', bty='n', col=c('black', 'red'), lwd=3, legend=c('census', 'effective'))
dev.off()

#------------------------------------------------------------------------
#Coalescence probability of two lineages
#------------------------------------------------------------------------
coalprob<-function(t, N=1000){
  return((1-1/(2*N))^t*1/(2*N));
}

pdf("probdist_TMRCA_2.pdf", width=4, height=4.3)
par(las=1, oma=c(0,0,0,0), mar=c(4,4,1.0,0.5));
N<-1000000;
t<-seq(1,8*N, length.out=200)

plot(t, coalprob(t, N), xlab="Generations", ylab="Probability of coalescence", xaxt='n', yaxt='n', type='l', lwd=3, col='#ff9c00', xaxs='i', ylim=c(0,1/(2*N)), yaxs='i')
#add mean
y<-par("usr")[4]*0.8
lines(rep(2*N,2), c(par("usr")[3], y), lty=2, lwd=2)
text(2*N, y, labels="Mean", pos=3);

#add axis
axis(side=1, at=c(1, 2*N, 4*N, 6*N, 8*N), labels=c(1, '2N', '4N', '6N', '8N'))
axis(side=2, at=c(0, 1/(2*2*N), 1/(2*N)), labels=expression(0, frac(1, "4N"), frac(1, "2N")))


dev.off()
#------------------------------------------------------------------------
#Expected heterozygosity in a Population
#------------------------------------------------------------------------

pdf("Expected_heterozygosity.pdf", width=3, height=3.25)
par(las=1, oma=c(0,0,0,0), mar=c(4,4,0.5,1));

logTheta <- seq(-3, 3, length.out=100)
theta <- 10^(logTheta)
human <- 4 * 8 * 10^9 * 10^-8;

plot(0, type='n', ylim=c(0, 1), ylab="Expected heterozygosity", xlab=expression(theta), xaxt='n', xaxs='i', xlim=c(min(logTheta), max(logTheta)));

lines(logTheta, theta/(theta+1), lwd=2, col='black');
lines(rep(log10(human),2), par("usr")[3:4], lwd=2, col='orange', lty=2)
lines(par("usr")[1:2],rep(human/(human+1),2), lwd=2, col='orange', lty=2)
points(log10(human), human/(human+1), pch=19, col='orange');

axis(side=1, at=(-3):3, labels=expression(10^-3, 10^-2, 10^-1, 1, 10^1, 10^2, 10^3));
dev.off()

#------------------------------------------------------------------------
#Error of ignoring 1/4n^2 terms
#------------------------------------------------------------------------
getprob<-function(N=1000, k=5){
  x <- 0;
  for(i in 1:(k-2)){
    for(j in (i+1):(k-1)){    
      x <- x + i*j
    }
  }
  a <- k*(k-1)/2/2/N;
  b <- a - x/4/N/N;
  return(c(a,b))
}

logN <- seq (2,4, length.out=100);
N <- 10^logN;
k<-c(5,10,20)

pdf("error_1_by_4Nsquare_terms.pdf", width=6, height=4.5);
par(las=1, oma=c(0,0,0,0), mar=c(4,4,0.5,1.2));
plot(0, type='n', xlab="Population size N", ylab="P(at least one coalescent event)", xlim=c(min(logN), max(logN)), ylim=c(0,0.5), xaxs='i', yaxs='i', xaxt='n', las=1)
#axis(side=1, at=2:4, labels=expression(10^2, 10^3, 10^4));
l <- c(100, 200, 500, 1000, 2000, 5000, 10000);
axis(side=1, at=log10(l), labels=l) ;

res <- matrix(data=0, ncol=2, nrow=length(N));
myCol <- c('black', 'orange', 'blue')
for(j in 1:length(k)){
  
  for(i in 1: length(N)){
    res[i,] <- getprob(N[i], k[j]);
  }
  lines(logN, res[,1], col=myCol[j], lwd=2)
  lines(logN, res[,2], col=myCol[j], lty=2, lwd=2)  
}

legend('right', bty='n', legend=paste("k =", k), col=myCol, lwd=2);
#legend('top', bty='n', legend=c("only 1/2N", "also 1/4N^2"), col='black', lwd=2, lty=c(1,2));
legend('topright', bty='n', legend=expression("only " ~ frac(1,'2N') ~ " terms", frac(1,'2N') ~ " and " ~ frac(1,'4N'^2) ~ " terms"), col='black', lwd=2, lty=c(1,2), y.intersp=2);

dev.off();
#------------------------------------------------------------------------
#Coalescence probability of multiple lineages
#------------------------------------------------------------------------
exptime<-function(n, maxcat){
  totH<-2*(1-1/n);
  ret<-rep(0, maxcat+1);
  h<-1/choose(2:(min(n, maxcat)),2)
  ret[1:length(h)]<-h;
  ret[maxcat]<-totH-sum(h);
  #now make cumulative
  for(i in (length(ret)-1):1){
    ret[i]<-ret[i]+ret[i+1];
  }
  return(ret);
}
maxcat<-6;
n_log<-seq(1,10, length.out=50)
n<-round(2^n_log)
res<-matrix(0, nrow=length(n), ncol=maxcat+1);
for(i in 1:length(n)){
  res[i,]<-exptime(n[i], maxcat);
}
g<-seq(0.45,0,length.out=floor((maxcat)/2));
col<-data.frame("r"=seq(1,0,length.out=maxcat-1), 'g'=c(g,rev(g[-length(g)])), 'b'=seq(0,1,length.out=maxcat-1));
col<-c(rgb(col), 'gray');


pdf("TreeHeight.pdf", width=4, height=4.5)
par(las=1, oma=c(0,0,0,0), mar=c(4,4.2,0.5,1));
plot(0, type='n', xlim=c(1,max(n_log)), ylim=c(0, 2), xlab="Sample size", ylab=expression("Expected time with j lineages t"[j]), xaxt='n', xaxs='i', yaxs='i', yaxt='n')
for(i in 1:maxcat){
  polygon(c(n_log, rev(n_log), n_log[1]), c(res[,i], rev(res[,i+1]), res[1,i]), col=col[i], border=NA);
  if(i<maxcat){
    text(par("usr")[2], (res[length(n),i]+res[length(n),i+1])/2, pos=2, labels=paste("j=", i+1, sep=""));
  } else {
    text(par("usr")[2], (res[length(n),i]+res[length(n),i+1])/2, pos=2, labels=paste("j>", maxcat, sep=""));
  }
}
axis(side=1, at=1:10, labels=2^(1:10))
axis(side=2, at=seq(0,2,by=0.5), labels=c(0, "N", "2N", "3N", "4N"))

lines(par("usr")[1:2], rep(1,2), lty=2)
lines(par("usr")[1:2], rep(1,2), lty=2)
lines(par("usr")[1:2], rep(2/maxcat,2), lty=2)
dev.off()

#------------------------------------------------------------------------
#tree height vs tree length
#------------------------------------------------------------------------
getHeight<-function(k, N){
  return(4*(1-1/k));
}
getLength<-function(k, N){
  ret<-numeric(length(k));
  for(i in 1:length(k)){
    ret[i]<-4*sum(1/(2:k[i]-1));
  }
  return(ret);
}

pdf("TreeHeightVsLength.pdf", width=4.5, height=3.5)
par(las=1, oma=c(0,0,0,0), mar=c(4,4,0.5,1));
n_log<-seq(1,9, length.out=50)
n<-round(2^n_log)
N<-100000;
plot(0, type='n', xlim=c(1,max(n_log)), ylim=c(0, 28), xlab="Sample size n", ylab=expression("E[" ~'L'[n] ~"]  or  E[" ~'T'[n] ~"]"), xaxt='n', xaxs='i', yaxs='i', yaxt='n')

lines(n_log, getHeight(n, N) , col='black', lwd=3);
lines(n_log, getLength(n, N) , col='red', lwd=3);

axis(side=1, at=1:10, labels=2^(1:10))
l<-seq(0,28,by=4);
axis(side=2, at=l, labels=paste(l, "N", sep=""))
lines(par("usr")[1:2], rep(4,2), lty=2)
legend('topleft', bty='n', col=c('red', 'black'), legend=expression("E["~ 'L'[n] ~ "]","E["~ 'T'[n] ~ "]"), lwd=3)
dev.off()

#------------------------------------------------------------------------
#Example SFS
#------------------------------------------------------------------------
a<-c(2,2,1,0); #a<-a/sum(a);

pdf("example_SFS.pdf", width=3, height=2.8);
par(las=1, oma=c(0,0,0,0), mar=c(5,4,0.5,0.1), mgp=c(2.5, 1, 0));
barplot(a, names.arg=1:4, xlab="Derived allele count f", ylab="Proportion", col='orange2', border=NA, space=0.8, axes=F);
axis(side=2, at=c(0,1,2), labels=c(0,1,2))
dev.off();

#------------------------------------------------------------------------
#Expected SFS
#------------------------------------------------------------------------

pdf("Expected_SFS.pdf", width=3.9, height=3)


par(las=1, oma=c(0,0,0,0), mar=c(4,4,0.5,1));
n<-10
f <- 1/1:n; f <- f/sum(f);

barplot(f, names.arg=1:n, xlab="Derived allele count f", ylab="Proportion", col='orange2', border=NA, space=0.8, axes=T);



#plot(0, type='n', xlim=c(1,n), ylim=c(0, 1), ylab=expression(paste("Expected number of mutations [", theta, "]")), xlab="Allele carriers", xaxt='n', yaxt='n')
#x<-1:(n[i]-1);  
#lines(1:n, 1/(1:n), type='b', pch=19, lwd=3, col='orange');

#l<-c(1, seq(5,n, by=5))
#axis(side=1, at=l, labels=l);
#l<-seq(0,1,by=0.2);
#axis(side=2, at=l, labels=l)


dev.off()

