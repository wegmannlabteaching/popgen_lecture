
getprob<-function(N=1000, k=5){
  x <- 0;
  for(i in 1:(k-2)){
    for(j in (i+1):(k-1)){    
      x <- x + i*j
    }
  }
  a <- k*(k-1)/2/2/N;
  b <- a - x/4/N/N;
  return(c(a,b))
}

logN <- seq (2,4, length.out=100);
N <- 10^logN;
k<-c(5,10,20)

pdf()
plot(0, type='n', xlab="Population size N", ylab="P(at least one coalescent event)", xlim=c(min(logN), max(logN)), ylim=c(0,0.5), xaxs='i', yaxs='i', xaxt='n', las=1)
#axis(side=1, at=2:4, labels=expression(10^2, 10^3, 10^4));
l <- c(100, 200, 500, 1000, 2000, 5000, 10000);
axis(side=1, at=log10(l), labels=l) ;

res <- matrix(data=0, ncol=2, nrow=length(N));
myCol <- c('black', 'orange', 'blue')
for(j in 1:length(k)){
  
  for(i in 1: length(N)){
    res[i,] <- getprob(N[i], k[j]);
  }
  lines(logN, res[,1], col=myCol[j], lwd=2)
  lines(logN, res[,2], col=myCol[j], lty=2, lwd=2)  
}

legend('right', bty='n', legend=paste("k =", k), col=myCol, lwd=2);
#legend('top', bty='n', legend=c("only 1/2N", "also 1/4N^2"), col='black', lwd=2, lty=c(1,2));
legend('topright', bty='n', legend=expression("only " ~ frac(1,'2N') ~ " terms", frac(1,'2N') ~ " and " ~ frac(1,'4N'^2) ~ " terms"), col='black', lwd=2, lty=c(1,2), y.intersp=2);

