\documentclass[9pt]{beamer}

\usepackage{../lectures}

\lectureTitle{Coalescent Theory}

%TOPIC Coalescent Theory with two individuals / chromosomes
%TOPIC Effective population sizes
%TOPIC Coalescent Theory with multiple individuals
%READING Chapter 3 (pages 35 - 40)
%EXERCISES Exercises_2

\begin{document}
\maketitle

%-----------------------------------------------------------------------------------------
\section{Recap}

\begin{frame}
\frametitle{Mutation}


 \begin{itemize}
 \item Over time, genetic drift is reducing genetic diversity.
 \item The reason we still observe genetic diversity are new mutations.
 \item Mutations are very rare. Single nucleotide polymorphisms (SNPs) occur with a rate of about $\mu=1.2\cdot10^{-8}$ per generation per nucleotide position.
 \item But this still mounts to $\sim$75 mutations per individual per generation.
 \item Given the large number of people, most mutations occur every generation.

 \pause
 \item Recurrent mutations do not affect allele frequencies much: we will thus ignore their effect in the future.
 \item The probability of fixation is equal to the allele frequency. New mutations thus fix with a probability of $\frac{1}{2N}$
 \item Since the number of new mutations is $2N\mu$, the number of fixing mutations is independent of the population size $\Rightarrow$ molecular clock!
 \end{itemize}
 
 \pause
 \bigskip
  \textbf{Question:} How does this translate into differences between individuals?


\end{frame}
%-----------------------------------------------------------------------------------------
 \section{Coalescent Theory: two samples}
\begin{frame}
  \frametitle{Intro to Coalescent Theory}

  \begin{columns}[T]
    \begin{column}{4.5cm}
     \includegraphics[width=\columnwidth]{Figures/coalescent_n2.pdf}

     \end{column}
    \begin{column}{7cm}
     \textbf{Coalescent theory}\\
  A population genetic theory that considers the history of a sample \textbf{backward in time}.\\\medskip

  \textbf{Coalescent event}\\
  If two sampled lineages have the same parent in the previous generation.\\\medskip

  \pause

  \textbf{Probability to coalesce}\\
  Under random mating, two lineages coalesce in the previous generation with probability
  $$\P(\mbox{2 individuals coalesce}) = \frac{1}{2N}$$

  \pause
  Expected time $t_2$ until two lineages coalesce (time to the Most Recent Common Ancestor, MRCA):
  $$\E[t_2] = 2N \mbox{ generations.}$$

    \end{column}
  \end{columns}

  \end{frame}
  %-----------------------------------------------------------------------------------------
  \begin{frame}
   \frametitle{Intro to Coalescent Theory}

   \textbf{Time to MRCA}\\
   While the expected time for two lineage to coalesce in $2N$ generations, there is a large variance associated with this expectations.

   \bigskip
   \pause

   \begin{columns}
    \begin{column}{4.5cm}
   The probability that two lineages coalesce $t$ generations ago is
   $$\P(t_2=t)=\left(1-\frac{1}{2N}\right)^{t-1}\frac{1}{2N}$$
   
   Using the approximation
   $$(1-x)^t \approx e^{-xt}$$

   we can rewrite this as

   $$\P(t_2=t)=e^{-\frac{1}{2N}(t-1)}\frac{1}{2N} $$
    \end{column}
   \begin{column}{5.3cm}
   
   \includegraphics[width=\columnwidth]{Figures/probdist_TMRCA_2.pdf}
   \end{column}
    \end{columns}
  \end{frame}

  %-----------------------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Mutations on the Coalescent}
  \textbf{Expected number of mutations between two samples}\\

   \begin{columns}
    \begin{column}{6.8cm}
      If the mutation rate per generation per lineage is $\mu$, the expected number of mutational differences $d_{ij}$ between two samples $i$ and $j$ is thus $$\E[d_{ij}]=2\cdot \E[t_2] \mu = 4N\mu$$

      \pause
      \textbf{Note:} the population size $N$ and the mutation rate $\mu$ have the same effect on genetic diversity!
   \end{column}
   \begin{column}{3cm}
      \onslide<1->\includegraphics[width=\columnwidth]{Figures/mutation_n2.pdf}
   \end{column}
  \end{columns}

  \medskip
  \pause

  \begin{alertblock}{Definition: $\theta$}
   $\theta := 4N\mu$ is an important population genetics parameter characterizing the expected genetic diversity of a population.
  \end{alertblock}

  \end{frame}
    %-----------------------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Estimating $\theta$}

  \textbf{Infinite Sites Model (ISM)}\\
  Assumes that each mutation hits a different base pair. This is realistic if $\theta \ll 1$.\\\medskip

  \textbf{The Tajima estimator of $\theta$ under ISM}\\
  Since we expect $\theta=4N\mu$ mutations between two samples, the average number of differences $\pi$ between individuals is an estimate of $\theta$.\\\medskip

  $$\hat{\theta}_T = \pi = \displaystyle \frac{1}{n(n-1)/2}\sum_{i=1}^{n-1}\sum_{j=i+1}^{n}d_{ij},$$

   where $n(n-1)/2$ is the number of pairs among $n$ samples.\\\bigskip

  \pause
  \textbf{Note:} While $\E[\pi] = \theta$, any particular value of $\pi$ will unlikely be exactely  $ \theta$!\\\medskip\pause
  This is why we call $\pi$ and \emph{estimator} of $\theta$ and write it as $\hat{\theta}$ or $\hat{\theta}_T$ to indicate that it is the \emph{Tajima} estimator.

  \end{frame}
      %-----------------------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Estimating $\theta$}

   \begin{block}{Example: Sequence data from four samples (lineages)}
   \texttt{Sample 1: aggtatgcta gaaccctaga aagacacaga gatagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 2: agg\textcolor{red}{c}atgcta gaaccctaga aagac\textcolor{red}{g}caga gatagacaag tgttcattca}\\
   \texttt{Sample 3: agg\textcolor{red}{c}atgcta gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}\\
   \texttt{Sample 4: aggtatgct\textcolor{red}{g} gaaccctaga aagacacaga \textcolor{red}{t}atagacaag tgtt\textcolor{red}{a}attca}
  \end{block}

  \pause

  \begin{eqnarray*}
  \hat{\theta}_T = \pi = \displaystyle \frac{1}{n(n-1)/2}\sum_{i=1}^{n-1}\sum_{j=i+1}^{n}d_{ij} \pause &=& \frac{d_{12}+d_{13}+d_{14}+d_{23}+d_{24}+d_{34}}{6}\\\pause
    &=& \frac{3+2+2+3+5+2}{6} = \frac{17}{6} = 2.83
  \end{eqnarray*}

  \pause

  \textbf{Note:} to compare numbers between studies, it is best to report $\pi$ / site:

  \begin{equation*}
  \hat{\theta}_T = \pi = \frac{3+2+2+3+5+2}{6}\cdot \frac{1}{50} = \frac{17}{6\cdot 50} = \frac{17}{300} = 0.057
  \end{equation*}



  \end{frame}
%-----------------------------------------------------------------------------------------
\begin{frame}
  \frametitle{Expected Heterozygosity}

  \begin{itemize}
   \item If two lineages coalesce before a mutation occurred on either lineage, the two samples are identical by state.
   \item If a mutation arises before a coalescent event, then the samples will differ.
  \end{itemize}

   \medskip
  \pause

\begin{columns}[T]
 \begin{column}{5.5cm}
\textbf{Expected heterozygosity}\\
The expected heterozygosity in a population can thus be derived as
\begin{eqnarray*}
\displaystyle E[H] &= \displaystyle \frac{P(\mbox{mutation})}{P(\mbox{mutation}) + P(\mbox{coalescence})}\\ \pause &=  \displaystyle \frac{2\mu}{2\mu + \frac{1}{2N}} = \frac{4N\mu}{4N\mu+1}=\frac{\theta}{\theta+1}
\end{eqnarray*}
\pause
\textbf{Example: humans}\\
$\theta \approx 4\cdot 8\cdot 10^9 \cdot 10^{-8} = 320$\\
$\E[H] = 320/321 = 0.997$
 \end{column}
 \begin{column}{4.5cm}
 \onslide<4->\includegraphics[width=\columnwidth]{Figures/Expected_heterozygosity.pdf}
 \end{column}
\end{columns}
\bigskip
\pause
\textbf{Question:} why are we not all heterozygous at $99.7$\% of all sites? (Heterozygosity in humans is $\approx 0.1\%$)


 \end{frame}

  %-----------------------------------------------------------------------------------------
  \section{Effective population size}
\begin{frame}
\frametitle{Effective population sizes}
All these theoretical results are for an idealized, simple model. Yet, we can force it to work for many non-standard models.

\begin{block}{The concept of effective population size ($N_e$)}
{\em Effective population size}: An adjusted population size we can use under a WF-model to accurately predict properties of non-WF populations.
\end{block}

\begin{example}[Varying population size]
If population sizes vary through time as: $N_0, N_1, N_2, \ldots, N_t$, a WF-model with $N_e$ as the harmonic mean
$$ N_e = 1/ \left[ \frac{1}{t}\left(\frac{1}{N_0}+\frac{1}{N_1}+\ldots+\frac{1}{N_{t-1}}\right)\right ] $$
still predicts, for example, the decrease in heterozygosity through time perfectly.
\end{example}

 \end{frame}

 %-----------------------------------------------------------------------------------------
 \begin{frame}
\frametitle{Effective population sizes}


\begin{example}[Unequal sex ratio]
For a population with unequal numbers of males and females, a WF-model with
$$ N_e = \displaystyle \frac{4N_mN_f}{N_m+N_f} $$
still predicts, for example, the expected heterozygosity perfectly.
\end{example}

\bigskip \pause

\begin{columns}
 \begin{column}{6.5cm}
\textbf{Example: Elephant Seals (\textit{Mirounga leonina})}\\
 Mating system: harem polygyny.\\
 On the Falkland Islands in one season, $N_m=28$ males but $N_f=550$ females were reproducing. 


$$N_e = \displaystyle \frac{4N_mN_f}{N_m+N_f} = \displaystyle \frac{4\cdot28\cdot550}{28+550} = 106.6 $$
  
  \smallskip

  Hence, heterozygosity in this population is similar to that of a WF population with 53 males and 53 femals.
  
 \end{column}
\begin{column}{3.5cm}
 \includegraphics[width=\columnwidth]{Figures/elephantSeal1.jpg}
\end{column}
\end{columns}


 \end{frame}


 %-----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Effective population sizes}

 \textbf{The effective population size of humans}\\\bigskip


 \begin{columns}
 \begin{column}{4cm}
   The effective size $N_e$ can be directly estimated from an estimate of $\theta$ and knowledge about the mutation rate.\\


 $$ \theta = 4N_e\mu \Rightarrow N_e = \frac{\theta}{4\mu}$$

 We assume $\mu = 1.2 \cdot 10^{-8}$.
 \end{column}
 \begin{column}{5.8cm}
   \begin{tabular}{l l c c}
 \hline
 Chr & Population & $\pi = \hat{\theta}$ & $\hat{N_e}$\\
 \hline
 Auto & Mandenka & 0.00120 & 25,000\\
           & Biaka & 0.00121 & 25,200\\
           & Basque & 0.00087 & 18,100\\
 X & Mandenka & 0.00099 & 20,600\\
              & Biaka & 0.00095 & 19,800\\
              & Basque & 0.00071 & 14,800\\
 \hline
\end{tabular}
 \end{column}
 \end{columns}

 \bigskip
 \pause
 \textbf{Note:} the effective size of human populations is much lower than their census sizes due to their demographic histories.\\ \medskip
 \pause
 \textbf{Note:} the effective size of the X chromosome is about $\frac{3}{4}$ of the autosomes as there are only 3 X per 4 autosomal copies in the population.


 \end{frame}

 %-----------------------------------------------------------------------------------------
 \section{Coalescent Theory: multiple samples}
 \begin{frame}
  \frametitle{Coalescence with multiple samples}

  \textbf{What is the probability of coalescence per generation among $k$ lineages?}\\
  There are many possibilities coalescent events: lineages (1,2), (2,5), (1,2,4), $\ldots$\\\medskip

  \textbf{Trick:} $P($at least one coalescent event$)=1-P($no coalescent event$)$


  \begin{align*}
   \P(\mbox{no coalescence}) &= \frac{2N}{2N} \cdot \frac{2N-1}{2N} \cdot \frac{2N-2}{2N} \ldots \frac{2N-k+1}{2N}\\
   \onslide<2->{&= 1\left(1-\frac{1}{2N}\right)\left(1-\frac{2}{2N}\right) \ldots \left(1-\frac{k-1}{2N}\right)= \prod_{i=1}^{k-1}\left(1-\frac{i}{2N}\right)\\}
   \onslide<3->{&=1-\frac{1}{2N}-\frac{2}{2N} \ldots -\frac{k-1}{2N} + O\left(\frac{1}{4N^2}\right),}
  \end{align*}\\\medskip
  \onslide<4->{
  where $O(\frac{1}{4N^2})$ indicates all terms of the order $\frac{1}{4N^2}$ or smaller (e.g. $\frac{1}{8N^4}$).\\\medskip}
  \onslide<5->{
  As these terms become negligible if $N$ is large, we get:
  $$\P(\mbox{at least one coalescent event}) \approx 1 - \left(1-\displaystyle\sum_{i=1}^{k-1}\frac{i}{2N}\right) = \frac{k(k-1)}{2}\frac{1}{2N} = {k \choose 2}\frac{1}{2N}$$
  }
 \end{frame}

  %-----------------------------------------------------------------------------------------
  \begin{frame}
  \frametitle{Coalescence with multiple samples}
   $$\P(\mbox{at least one coalescent event}) = \frac{k(k-1)}{2}\frac{1}{2N} = {k \choose 2}\frac{1}{2N}$$

  \textbf{Intuitive explanation}\\
  Probability of coalescence among $k$ lineages = probability of coalescence among two lineages $\frac{1}{2N}$ times the number of possible pairs ${k \choose 2}$.
  
  \bigskip
  
  \pause

  \begin{columns}[T]
   \begin{column}{4cm}
  \textbf{Ignoring terms $O(\frac{1}{4N^2})$}\\
    Does this matter?\\\medskip

    \pause

    \begin{alertblock}{}
     The approximation holds well when $N$ is large and $k \ll N$.
    \end{alertblock}


   \end{column}
  \begin{column}{6cm}
    \includegraphics<2->[width=\columnwidth]{Figures/error_1_by_4Nsquare_terms.pdf}
  \end{column}
 \end{columns}

\end{frame}

%-----------------------------------------------------------------------------------------
\section{Summary}
\begin{frame}
 \frametitle{Summary}

 \begin{itemize}
 \item Coalescent theory is a population genetic theory that looks at the world \emph{backward in time}.
 \item This makes many calculations much simpler.

 \pause

    \item The probability that two individuals coalesce is $\frac{1}{2N}$ per generation.
    \item Two individuals differ on average by $\theta = 4N\mu$ mutations $\Rightarrow$ we can thus use the average pairwise differences as an estimate of $\theta$
    \item Coalescent theory applies to many non WF models when using an \emph{effective population size}

    \pause
    \item The probability of coalescence among $k$ lineages is ${k \choose 2}\frac{1}{2N}$, which we can use to calculate characteristics of the expected genealogy.
 \end{itemize}

 \pause
 \begin{block}{}
  For next week:\\ please read \textbf{Chapter 3 (pages 49 - 56)} and solve \textbf{Exercises 3}.
 \end{block}

\end{frame}

 %-----------------------------------------------------------------------------------------
\end{document}
